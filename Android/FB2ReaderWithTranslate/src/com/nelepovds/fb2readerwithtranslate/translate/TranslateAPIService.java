package com.nelepovds.fb2readerwithtranslate.translate;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.URLEncoder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import com.nelepovds.fb2readerwithtranslate.R;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

public class TranslateAPIService {

	private Context context;
	private String yandexAPIKey;

	public static final String FB2RWT_GET_LANGUAGES = "https://translate.yandex.net/api/v1.5/tr.json/getLangs?key=";
	public final int FB2RWT_LANGUAGES = 1;

	public static final String FB2RWT_GET_TRANSLATE = "https://translate.yandex.net/api/v1.5/tr.json/translate?key=";
	public final int FB2RWT_TRANSLATE = 2;

	public static interface ITranslateAPIServiceListener {

		public void getLangs(String langs);

		public void translatedText(String translatedText);

	}

	private ITranslateAPIServiceListener serviceListener;
	private Handler uiHandler;
	private TextView textProgress;
	private AlertDialog progressDialog;

	public TranslateAPIService(Context pContext, String pYandexAPIKey,
			ITranslateAPIServiceListener pServiceListener) {
		this.yandexAPIKey = pYandexAPIKey;
		this.context = pContext;
		this.serviceListener = pServiceListener;

		this.uiHandler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				if (serviceListener != null) {
					if (msg.what == FB2RWT_LANGUAGES) {
						serviceListener.getLangs((String) msg.obj);
					} else if (msg.what == FB2RWT_TRANSLATE) {
						serviceListener.translatedText((String) msg.obj);
					}

					progressDialog.dismiss();
				}
			}
		};
	}

	public void setServiceListener(ITranslateAPIServiceListener serviceListener) {
		this.serviceListener = serviceListener;
	}

	public String makeApiRequest(String urlForRequest, String apiKey,
			String params) throws ClientProtocolException, IOException {
		String retResponse = "";

		HttpParams httpParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParams, 15000);
		HttpConnectionParams.setSoTimeout(httpParams, 15000);
		DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);
		String uri = urlForRequest + apiKey + params;

		HttpGet httpGet = new HttpGet(uri);

		HttpResponse response = httpClient.execute(httpGet);
		HttpEntity entity = response.getEntity();
		if (entity != null) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			DataInputStream dis = new DataInputStream(entity.getContent());
			byte[] buffer = new byte[1024];// In bytes
			int realyReaded;
			while ((realyReaded = dis.read(buffer)) > -1) {
				baos.write(buffer, 0, realyReaded);
			}
			retResponse = baos.toString();
		}
		return retResponse;

	}

	private void createLoadingAlert(String text) {
		LinearLayout ll = new LinearLayout(context);
		ProgressBar pb = new ProgressBar(context);
		this.textProgress = new TextView(context);
		this.textProgress.setText(text);
		ll.addView(pb);
		ll.addView(this.textProgress);
		this.progressDialog = new AlertDialog.Builder(context)
				.setCancelable(false).setView(ll).create();
		this.progressDialog.show();

	}

	public void getLangs(final String langCode) {
		this.createLoadingAlert(context.getString(R.string.action_settings));
		new Thread(new Runnable() {
			public void run() {
				try {
					String retString = makeApiRequest(FB2RWT_GET_LANGUAGES,
							yandexAPIKey, "&ui=" + langCode);
					uiHandler.sendMessage(uiHandler.obtainMessage(
							FB2RWT_LANGUAGES, retString));
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}).start();
	}

	public void translate(final String selectedText, final String langSource,
			final String langDest) {
		this.createLoadingAlert(context.getString(R.string.translate));
		new Thread(new Runnable() {
			public void run() {
				try {
					String retString = makeApiRequest(FB2RWT_GET_TRANSLATE,
							yandexAPIKey,
							"&lang=" + langSource + "-" + langDest + "&text="
									+ URLEncoder.encode(selectedText));
					uiHandler.sendMessage(uiHandler.obtainMessage(
							FB2RWT_TRANSLATE, retString));
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}).start();

	}
}
