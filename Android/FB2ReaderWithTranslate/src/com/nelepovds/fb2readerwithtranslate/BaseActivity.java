package com.nelepovds.fb2readerwithtranslate;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Color;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

public class BaseActivity extends Activity {
	protected AlertDialog progressInfoDialog;
	protected TextView mTextProgress;

	protected void createProgressDialog(String message) {
		if (this.progressInfoDialog != null) {
			this.progressInfoDialog.dismiss();
		}
		LinearLayout ll = new LinearLayout(this);
		ProgressBar pb = new ProgressBar(this);
		this.mTextProgress = new TextView(this);
		this.mTextProgress.setText(message);
		this.mTextProgress.setTextColor(Color.WHITE);
		this.mTextProgress.setGravity(Gravity.CENTER_VERTICAL);
		ll.addView(pb);
		ll.addView(this.mTextProgress, LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.MATCH_PARENT);
		this.progressInfoDialog = new AlertDialog.Builder(this)
				.setCancelable(true).setView(ll).create();
		this.progressInfoDialog.show();

	}
}
