package com.nelepovds.fb2readerwithtranslate;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.ext.DefaultHandler2;

import com.nelepovds.fb2readerwithtranslate.tools.FB2ReadTools;

public class FB2XmlParser {

	public static void beginParse(File fileToRead,
			DefaultHandler2 saxParseHandler) {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser parser = null;
		try {
			parser = factory.newSAXParser();
			FileInputStream fis = new FileInputStream(fileToRead);
			InputSource is = new InputSource(new InputStreamReader(fis,
					FB2ReadTools.getEncodingFile(fileToRead)));
			parser.parse(is, saxParseHandler);
		} catch (ParserConfigurationException e) {
			// e.printStackTrace();
		} catch (SAXException e) {
			// e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static final String FB2_BINARY_DATA_NAME = "binary";

	public interface IFB2XmlParserGetImageListener {
		public void getImageBase64Content(String imageId, String contentBase64);
	}

	public static void findFbImageByID(File file, final String imageId,
			final IFB2XmlParserGetImageListener listener) {
		beginParse(file, new DefaultHandler2() {
			private String imageFoundedID = null;
			private StringBuilder imageFoundedContent = null;

			@Override
			public void startElement(String uri, String localName,
					String qName, Attributes attributes) throws SAXException {
				if (qName.equalsIgnoreCase(FB2_BINARY_DATA_NAME)) {
					if (attributes.getValue("id").equalsIgnoreCase(imageId)) {
						imageFoundedID = imageId;
						imageFoundedContent = new StringBuilder();
					}
				}
			}

			@Override
			public void characters(char[] ch, int start, int length)
					throws SAXException {
				if (imageFoundedID != null) {
					imageFoundedContent.append(new String(ch, start, length));
				}
			}

			@Override
			public void endElement(String uri, String localName, String qName)
					throws SAXException {
				if (qName.equalsIgnoreCase(FB2_BINARY_DATA_NAME)
						&& imageFoundedContent != null) {
					listener.getImageBase64Content(imageFoundedID,
							imageFoundedContent.toString());
					throw new SAXException("Image founded");

				}
			}

		});
	}

	public static final String FB2_SECTION_DATA_NAME = "section";

	public interface IFB2XmlParserSectionsListener {
		public void completeSection(Node sectionDom);

		public void completeParse();
	}

	public static void parseSections(File file,
			final IFB2XmlParserSectionsListener listener) {
		beginParse(file, new DefaultHandler2() {
			StringBuilder section = new StringBuilder();
			int level = 0;

			@Override
			public void endDocument() throws SAXException {
				super.endDocument();
				listener.completeParse();
			}

			@Override
			public void startElement(String uri, String localName,
					String qName, Attributes attributes) throws SAXException {
				if (qName.equalsIgnoreCase(FB2_SECTION_DATA_NAME)) {
					level++;
				}

				if (level > 0) {
					section.append("<");
					section.append(qName);
					if (qName.equalsIgnoreCase("image")) {
						for (int a = 0; a < attributes.getLength(); a++) {
							section.append(" ");
							section.append(attributes.getLocalName(a));
							section.append("=");
							section.append("\"");
							section.append(attributes.getValue(a));
							section.append("\"");

						}
					}
					section.append(">");
				}
			}

			@Override
			public void characters(char[] ch, int start, int length)
					throws SAXException {
				if (level > 0) {
					section.append(new String(ch, start, length));
				}
			}

			@Override
			public void endElement(String uri, String localName, String qName)
					throws SAXException {
				if (level > 0) {
					section.append("</");
					section.append(qName);
					section.append(">");
				}

				if (qName.equalsIgnoreCase(FB2_SECTION_DATA_NAME)) {
					if (level > 0) {
						level--;
					}
					if (level == 0 && section.length() > 0) {

						DocumentBuilderFactory factory = DocumentBuilderFactory
								.newInstance();
						DocumentBuilder builder;
						try {
							builder = factory.newDocumentBuilder();
							Node xmlSectionNode = builder.parse(
									new InputSource(new StringReader(section
											.toString()))).getFirstChild();
							listener.completeSection(xmlSectionNode);
						} catch (Exception e) {
							e.printStackTrace();
						}
						section = new StringBuilder();

					}
				}

			}
		});

	}
}
