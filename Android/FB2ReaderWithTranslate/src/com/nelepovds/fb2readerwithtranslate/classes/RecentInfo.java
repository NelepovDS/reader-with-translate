package com.nelepovds.fb2readerwithtranslate.classes;

import java.io.File;
import java.util.Date;

import com.nelepovds.fb2readerwithtranslate.App;

import android.database.sqlite.SQLiteDatabase;
import rainprod.utils.BaseObject;

public class RecentInfo extends BaseObject {
	public String file;
	public String name;
	public String cover;
	public String dateLastOpen;

	public String langSource;
	public String langDest;

	public Integer section = 0;
	public Integer totalSections = -1;
	public Integer scrollSectionY;
	public Integer fontSize = 14;

	public Integer ttsSpeed = 100;

	public String codePage;

	public Integer usePageBreaking = 1;

	public static RecentInfo fillBookInfo(SQLiteDatabase writableDataBase,
			File fileToRead, String bookDefaultName) {
		RecentInfo retInfo = new RecentInfo();
		retInfo.file = fileToRead.getAbsolutePath();
		retInfo.dateLastOpen = App.getTimeFormat(new Date());
		retInfo.section = 0;
		retInfo.scrollSectionY = 0;
		retInfo.name = bookDefaultName;

		retInfo.insert(writableDataBase, true);
		return retInfo;
	}
}
