package com.nelepovds.fb2readerwithtranslate;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.nelepovds.fb2readerwithtranslate.database.FB2DataBase;

import rainprod.utils.BaseApplication;

public class App extends BaseApplication {

	public static final String FB2RWT_LANG_SOURCE = "langSource";
	public String langSource = "";

	public static final String FB2RWT_LANG_DEST = "langDest";
	public String langDest = "";

	public static final String FB2RWT_TRANSLATED_TEXT_FONT_SIZE = "translatedTextFontSize";
	public String translatedTextFontSize = "12";

	public static final String FB2RWT_LANGUAGES = "langs";
	public JSONObject languages = null;

	public static final String FB2RWT_DIRS = "dirs";
	public ArrayList<String> dirs = new ArrayList<String>();

	private FB2DataBase dataBase = null;

	public static App getApp() {
		App retApp = (App) App.application;
		if (retApp.dataBase == null) {
			retApp.dataBase = new FB2DataBase(retApp);

		}
		return retApp;
	}

	public synchronized FB2DataBase getDataBase() {
		return this.dataBase;
	}

	@Override
	public void loadingPreferences() {
		this.loadDirs();
		this.loadLanguages();
		this.loadSourceAndDest();
		this.loadTranslatedTextFontSize();
	}

	private void loadTranslatedTextFontSize() {
		this.translatedTextFontSize = (String) this.readSettingValue(
				FB2RWT_TRANSLATED_TEXT_FONT_SIZE, this.translatedTextFontSize);
	}

	public void setTranslatedTextFontSize(String translatedTextFontSize) {
		this.translatedTextFontSize = translatedTextFontSize;
		this.writeSettingValue(FB2RWT_TRANSLATED_TEXT_FONT_SIZE,
				translatedTextFontSize);
	}

	public String getTranslatedTextFontSize() {
		return translatedTextFontSize;
	}

	private void loadSourceAndDest() {
		this.langSource = (String) this
				.readSettingValue(FB2RWT_LANG_SOURCE, "");
		this.langDest = (String) this.readSettingValue(FB2RWT_LANG_DEST, "");
	}

	public void setLangDest(String langDest) {
		this.langDest = langDest;
		this.writeSettingValue(FB2RWT_LANG_DEST, langDest);
	}

	public void setLangSource(String langSource) {
		this.langSource = langSource;
		this.writeSettingValue(FB2RWT_LANG_SOURCE, langSource);
	}

	private void loadDirs() {
		String strDir = (String) this.readSettingValue(FB2RWT_DIRS, "");
		try {
			JSONArray dirsTemp = new JSONArray(strDir);
			this.dirs.clear();
			for (int d = 0; d < dirsTemp.length(); d++) {
				dirs.add(dirsTemp.getString(d));
			}
		} catch (JSONException e) {

		}

	}

	private void loadLanguages() {
		String strLang = (String) this.readSettingValue(FB2RWT_LANGUAGES, "");
		try {
			this.languages = new JSONObject(strLang);
		} catch (JSONException e) {

		}

	}

	public void updateLanguagesInfo(String langs) throws JSONException {
		JSONObject jsonRet = new JSONObject(langs);
		JSONArray dirsTemp = jsonRet.getJSONArray(FB2RWT_DIRS);
		this.writeSettingValue(FB2RWT_DIRS, dirsTemp.toString());
		this.dirs.clear();
		for (int d = 0; d < dirsTemp.length(); d++) {
			dirs.add(dirsTemp.getString(d));
		}

		// Langs
		this.languages = jsonRet.getJSONObject(FB2RWT_LANGUAGES);
		this.writeSettingValue(FB2RWT_LANGUAGES, this.languages.toString());

	}

	public String getSectionTemplate() {
		return this.readFileFromAssets("FB2SectionTemplate.html");

	}

}
