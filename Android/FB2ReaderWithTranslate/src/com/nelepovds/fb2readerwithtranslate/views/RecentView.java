package com.nelepovds.fb2readerwithtranslate.views;

import com.nelepovds.fb2readerwithtranslate.classes.RecentInfo;

import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import rainprod.utils.ui.OSViewHolder;

public class RecentView extends OSViewHolder {

	private TextView txtName;
	private TextView txtDateLastOpen;
	private Button btnOpenRead;
	private TextView txtProgress;
	private ProgressBar progressBook;

	public static interface IRecentViewListener {

		public void openForRead(RecentInfo info);

	}

	private IRecentViewListener listener;

	public void setListener(IRecentViewListener listener) {
		this.listener = listener;
	}

	public RecentView(View convertView) {
		super(convertView);
	}

	@Override
	public void fillView(Object... params) {
		final RecentInfo info = (RecentInfo) params[0];

		this.txtName.setText(info.name);
		this.txtDateLastOpen.setText(info.dateLastOpen);
		this.btnOpenRead.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (listener != null) {
					listener.openForRead(info);
				}
			}
		});
		if (info.totalSections > 0) {
			this.txtProgress.setText(String.valueOf(info.section+1) + "/"
					+ String.valueOf(info.totalSections));

			this.progressBook.setMax(info.totalSections);
			this.progressBook.setProgress(info.section);
			this.txtProgress.setVisibility(View.VISIBLE);
			this.progressBook.setVisibility(View.VISIBLE);
		} else {
			this.txtProgress.setVisibility(View.GONE);
			this.progressBook.setVisibility(View.GONE);
		}
	}

}
