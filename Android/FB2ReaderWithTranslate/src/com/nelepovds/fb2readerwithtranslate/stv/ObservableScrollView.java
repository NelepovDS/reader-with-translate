package com.nelepovds.fb2readerwithtranslate.stv;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

import java.util.ArrayList;

/**
 * ScrollView with a onScrollChangedListener
 *
 * http://stackoverflow.com/questions/3948934/synchronise-scrollview-scroll-positions-android
 *
 */
public class ObservableScrollView extends ScrollView{

        private ArrayList<IOnScrollChangedListener> mOnScrollChangedListeners;
        
        @SuppressWarnings("unused")
        public ObservableScrollView(Context context) {
                super(context);
                init();
        }

        @SuppressWarnings("unused")
        public ObservableScrollView(Context context, AttributeSet attrs, int defStyle) {
                super(context, attrs, defStyle);
                init();
        }

        @SuppressWarnings("unused")
        public ObservableScrollView(Context context, AttributeSet attrs) {
                super(context, attrs);
                init();
        }

        private void init() {
                mOnScrollChangedListeners = new ArrayList<IOnScrollChangedListener>(2);
        }

        public void addOnScrollChangedListener(IOnScrollChangedListener onScrollChangedListener) {
                mOnScrollChangedListeners.add(onScrollChangedListener);
        }

        @SuppressWarnings("unused")
        public void removeOnScrollChangedListener(IOnScrollChangedListener onScrollChangedListener) {
                mOnScrollChangedListeners.remove(onScrollChangedListener);
        }
        
        /**
         * google should make this method public and add a setOnScrollChangedListener
         * override to allow listener
         */
        @Override
        protected void onScrollChanged(int x, int y, int oldx, int oldy) {
                super.onScrollChanged(x, y, oldx, oldy);
                for (IOnScrollChangedListener listener : mOnScrollChangedListeners) {
                        listener.onScrollChanged(this, x, y, oldx, oldy);
                }
        }
        
}