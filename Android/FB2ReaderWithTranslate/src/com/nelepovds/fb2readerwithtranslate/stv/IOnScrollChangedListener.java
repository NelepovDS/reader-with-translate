package com.nelepovds.fb2readerwithtranslate.stv;

public interface IOnScrollChangedListener {
	 public void onScrollChanged(ObservableScrollView scrollView, int x, int y, int oldx, int oldy);
}
