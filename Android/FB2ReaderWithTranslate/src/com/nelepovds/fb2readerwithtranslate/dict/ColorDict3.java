package com.nelepovds.fb2readerwithtranslate.dict;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.Gravity;

public class ColorDict3 {
	// http://blog.socialnmobile.com/2011/01/colordict-api-for-3rd-party-developers.html
	public static final String SEARCH_ACTION = "colordict.intent.action.SEARCH";
	public static final String EXTRA_QUERY = "EXTRA_QUERY";
	public static final String EXTRA_FULLSCREEN = "EXTRA_FULLSCREEN";
	public static final String EXTRA_HEIGHT = "EXTRA_HEIGHT";
	public static final String EXTRA_WIDTH = "EXTRA_WIDTH";
	public static final String EXTRA_GRAVITY = "EXTRA_GRAVITY";
	public static final String EXTRA_MARGIN_LEFT = "EXTRA_MARGIN_LEFT";
	public static final String EXTRA_MARGIN_TOP = "EXTRA_MARGIN_TOP";
	public static final String EXTRA_MARGIN_BOTTOM = "EXTRA_MARGIN_BOTTOM";
	public static final String EXTRA_MARGIN_RIGHT = "EXTRA_MARGIN_RIGHT";

	public static final String MARKET_SEARCH = "com.socialnmobile.colordict";

	public static void searchWordInDict(Context cntx, String word) {
		Intent intent = new Intent(SEARCH_ACTION);
		intent.putExtra(EXTRA_QUERY, word); // Search Query
		intent.putExtra(EXTRA_FULLSCREEN, false); //
		intent.putExtra(EXTRA_HEIGHT, 400);
		intent.putExtra(EXTRA_GRAVITY, Gravity.BOTTOM);

		cntx.startActivity(intent);
	}

	public static boolean isIntentAvailable(Context context, Intent intent) {
		final PackageManager packageManager = context.getPackageManager();
		return packageManager.queryIntentActivities(intent,
				PackageManager.MATCH_DEFAULT_ONLY).size() > 0;
	}

	public static void openMarketForInstall(Context context) {
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse("market://search?q=" + MARKET_SEARCH));
		context.startActivity(i);
	}

}
