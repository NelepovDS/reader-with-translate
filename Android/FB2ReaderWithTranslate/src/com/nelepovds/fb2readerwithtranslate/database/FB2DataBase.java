package com.nelepovds.fb2readerwithtranslate.database;

import rainprod.utils.database.AndroidUtilsDataBase;
import android.content.Context;

import com.nelepovds.fb2readerwithtranslate.classes.RecentInfo;

public class FB2DataBase extends AndroidUtilsDataBase {

	public FB2DataBase(Context context) {
		super(context);
	}

	@Override
	public void initTables() {
		this.dataBaseTables.add(new RecentInfo());
	}

	@Override
	public int getVersion() {
		return 5;
	}

	@Override
	public String getDataBaseName() {
		return "fb2rwt.sqlite";
	}

}
