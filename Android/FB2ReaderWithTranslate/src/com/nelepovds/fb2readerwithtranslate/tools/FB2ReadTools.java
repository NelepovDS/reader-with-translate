package com.nelepovds.fb2readerwithtranslate.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FB2ReadTools {

	static public String getEncodingFile(File fileToRead) {
		String retEncoding = "UTF-8";
		// encoding="(.*?)"
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(fileToRead));
			String xmlFirstFile = br.readLine();
			Matcher mat = Pattern.compile("encoding=\"(.*?)\"").matcher(
					xmlFirstFile);
			if (mat.find() && mat.groupCount() > 0) {
				retEncoding = mat.group(1);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {

					e.printStackTrace();
				}
			}
		}

		return retEncoding;
	}
}
