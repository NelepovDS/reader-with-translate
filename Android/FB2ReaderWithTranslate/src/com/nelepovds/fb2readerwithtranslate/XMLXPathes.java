package com.nelepovds.fb2readerwithtranslate;

import java.util.Iterator;

import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

public class XMLXPathes {

	@interface FB2ElementInfo {
		public String Name();
	}

	public static final String FB2RWT_BOOK_TITLE = "//*[local-name()='description']//*[local-name()='book-title']/text()";

	public static final String FB2RWT_COVER_IMAGE = "//coverpage//image/@href";
	public static final String FB2RWT_BINARY_ELEMENT_VALUE = "//*[local-name()='binary'][@id='%s']/text()";
	public static final String FB2RWT_SECTIONS = "//*[local-name()='section']";
	private static XPath xpath;

	public static XPath getXpath() {
		if (xpath == null) {
			xpath = XPathFactory.newInstance().newXPath();
			xpath.setNamespaceContext(new NamespaceContext() {

				@Override
				public String getNamespaceURI(String prefix) {
					return "http://www.w3.org/1999/xlink";
				}

				@Override
				public String getPrefix(String arg0) {
					return null;
				}

				@SuppressWarnings("rawtypes")
				@Override
				public Iterator getPrefixes(String arg0) {
					return null;
				}

			});
		}
		return xpath;
	}

	public static String getXPathString(Document doc, String expression) {
		String retString = null;
		try {
			retString = getXpath().compile(expression)
					.evaluate(doc, XPathConstants.STRING).toString();

		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return retString;
	}

	public static NodeList getXPathNodeList(Document doc, String expression) {
		NodeList retNodeList = null;
		try {
			retNodeList = (NodeList) getXpath().compile(expression).evaluate(
					doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return retNodeList;

	}
}
