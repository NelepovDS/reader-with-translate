package com.nelepovds.fb2readerwithtranslate.reader;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;

import rainprod.utils.ListOfObjects;
import rainprod.utils.database.DBFilter;
import rainprod.utils.ui.OSUserInterface;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.text.ClipboardManager;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.nelepovds.fb2readerwithtranslate.App;
import com.nelepovds.fb2readerwithtranslate.R;
import com.nelepovds.fb2readerwithtranslate.XMLXPathes;
import com.nelepovds.fb2readerwithtranslate.classes.RecentInfo;
import com.nelepovds.fb2readerwithtranslate.dialog.ReaderBookInfoDialog;
import com.nelepovds.fb2readerwithtranslate.dialog.ReaderBookInfoDialog.IReaderBookInfoDialogListener;
import com.nelepovds.fb2readerwithtranslate.dict.ColorDict3;
import com.nelepovds.fb2readerwithtranslate.reader.ReaderContent.IReaderContentListener;
import com.nelepovds.fb2readerwithtranslate.translate.TranslateAPIService;
import com.nelepovds.fb2readerwithtranslate.translate.TranslateAPIService.ITranslateAPIServiceListener;

@SuppressWarnings("deprecation")
public class ReaderActivity extends Activity implements
		IReaderBookInfoDialogListener, View.OnClickListener,
		ITranslateAPIServiceListener, OnInitListener {

	public static final String FB2RWT_FILE_PATH = ReaderActivity.class
			.getPackage() + ".filePath";
	public static final String FB2RWT_SECTION_TAG = ReaderActivity.class
			.getPackage() + ".Section_";
	protected static final int FB2RWT_SECTION_BUTTONS_ID = -100;

	// private RelativeLayout topBarHolder;

	private File fileToRead;
	private Button btnBack;

	// Scrolling buttons
	private ImageView imgTranslateAction;
	private ImageView imgShare;
	private ImageView imgBrightness;

	private SeekBar brightSeek;
	private SeekBar seekTTSSpeed;

	private Button btnApplyBright;
	private Button btnApplyFontSize;

	private ImageView imgTalk;
	private ImageView imgFontSize;
	private ImageView imgBookRead;
	private ImageView imgDictionaries;
	private ImageView imgSearch;
	private ImageView imgCopy;

	// TTS

	private String currentTag = "";

	private ReaderContent readerContent;

	// Sections
	private LinearLayout actionsHolder;
	private LinearLayout scrollViewLinearHolder;

	private Spinner actionLanguageSource;
	private Spinner actionLanguageDest;
	private Spinner spinFontSize;
	private Spinner spinTTSLocales;

	private Button btnVoiceSelectedText;
	private Button btnActionTranslate;

	private TranslateAPIService service;
	private TextToSpeech tts;
	protected String textInBuffer;
	private AlertDialog translatedTextDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_reader);
		OSUserInterface.initActivityControls(this);

		Window window = this.getWindow();
		window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		this.service = new TranslateAPIService(this,
				getString(R.string.yandexAPIKey), this);

		this.initControls();

		this.fileToRead = new File(getIntent().getExtras().getString(
				FB2RWT_FILE_PATH));
		ReaderBookInfoDialog readerBookDialog = new ReaderBookInfoDialog(this);
		readerBookDialog.setDialogListener(this);
		readerBookDialog.readFile(fileToRead);
	}

	private void initControls() {
		this.readerContent
				.setReaderContentListener(new IReaderContentListener() {

					@Override
					public void haveCopiedData(String string) {

						textInBuffer = string;
						runOnUiThread(new Runnable() {
							public void run() {
								Boolean isEnabled = textInBuffer.length() > 0;
								imgTranslateAction.setEnabled(isEnabled);
								imgShare.setEnabled(isEnabled);
								imgTalk.setEnabled(isEnabled);
								imgDictionaries.setEnabled(isEnabled);
								imgCopy.setEnabled(isEnabled);
								if (!isEnabled && tts != null
										&& tts.isSpeaking()) {
									tts.stop();
								}

								showOrHideActions("");
							}
						});

					}

					@Override
					public void sectionWasCompletlyLoaded() {

					}

				});

		this.imgTranslateAction.setOnClickListener(this);
		this.imgShare.setOnClickListener(this);

		this.imgBrightness.setOnClickListener(this);

		this.brightSeek.setProgress((int) Math
				.abs((getWindow().getAttributes().screenBrightness * 100)));
		this.brightSeek
				.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

					@Override
					public void onStopTrackingTouch(SeekBar seekBar) {
					}

					@Override
					public void onStartTrackingTouch(SeekBar seekBar) {
					}

					@Override
					public void onProgressChanged(SeekBar seekBar,
							int progress, boolean fromUser) {
						WindowManager.LayoutParams layout = getWindow()
								.getAttributes();
						layout.screenBrightness = progress / 100.0f;
						layout.screenBrightness = layout.screenBrightness < 0.1f ? 0.1f
								: layout.screenBrightness;
						getWindow().setAttributes(layout);

					}
				});
		this.btnApplyBright.setOnClickListener(this);

		this.imgTalk.setOnClickListener(this);
		this.btnVoiceSelectedText.setOnClickListener(this);
		// For now it's closing

		this.imgFontSize.setOnClickListener(this);
		this.btnApplyFontSize.setOnClickListener(this);
		this.fillFontSizes();
		this.imgBookRead.setOnClickListener(this);

		this.imgTranslateAction.setEnabled(false);
		this.imgShare.setEnabled(false);
		this.imgTalk.setEnabled(false);
		this.seekTTSSpeed
				.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

					@Override
					public void onStopTrackingTouch(SeekBar seekBar) {
						if (tts != null) {
							float ttsSpeed = seekBar.getProgress() / 100.0f;
							ttsSpeed = ttsSpeed < 0.1f ? 0.1f : ttsSpeed;
							tts.setSpeechRate(ttsSpeed);

							if (tts.isSpeaking()) {
								tts.stop();
								voiceSelectedText();
							}

						}
					}

					@Override
					public void onStartTrackingTouch(SeekBar seekBar) {

					}

					@Override
					public void onProgressChanged(SeekBar seekBar,
							int progress, boolean fromUser) {

					}
				});

		this.imgDictionaries.setEnabled(false);
		this.imgDictionaries.setOnClickListener(this);

		this.imgSearch.setOnClickListener(this);

		this.imgCopy.setEnabled(false);
		this.imgCopy.setOnClickListener(this);

		this.btnBack.setOnClickListener(this);

		this.btnActionTranslate.setOnClickListener(this);

		this.showOrHideActions("");

		this.loadSpinners();

	}

	@Override
	protected void onStart() {
		super.onStart();

		new Thread(new Runnable() {
			public void run() {
				tts = new TextToSpeech(ReaderActivity.this, ReaderActivity.this);
			}
		}).start();

	}

	private void fillFontSizes() {

		ArrayAdapter<Integer> adapterFonts = new ArrayAdapter<Integer>(this,
				android.R.layout.simple_spinner_item);
		for (int i = ReaderContent.FB2RWT_MIN_FONT_SIZE; i <= ReaderContent.FB2RWT_MAX_FONT_SIZE; i++) {
			adapterFonts.add(Integer.valueOf(i));
		}

		adapterFonts
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		this.spinFontSize.setAdapter(adapterFonts);
		this.spinFontSize
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int pos, long id) {
						if (readerContent != null) {
							readerContent.getmRecentInfo().fontSize = Integer
									.valueOf(spinFontSize
											.getItemAtPosition(pos).toString());
							readerContent.updateRecentInfo(readerContent
									.getmRecentInfo());
						}
					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {

					}
				});

	}

	private void loadSpinners() {
		JSONArray names = App.getApp().languages.names();
		String[] langs = new String[names.length()];
		for (int i = 0; i < names.length(); i++) {
			try {
				langs[i] = App.getApp().languages.getString(names.getString(i));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		Arrays.sort(langs, String.CASE_INSENSITIVE_ORDER);

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, langs);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		this.actionLanguageDest.setAdapter(adapter);
		this.actionLanguageSource.setAdapter(adapter);
		this.actionLanguageSource
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int pos, long id) {
						if (readerContent != null) {

							readerContent.getmRecentInfo().langSource = actionLanguageSource
									.getItemAtPosition(pos).toString();
							readerContent.updateRecentInfo(readerContent
									.getmRecentInfo());
							updateBookSettingsUI();
						}
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {
					}
				});
		this.actionLanguageDest
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int pos, long id) {
						if (readerContent != null) {
							readerContent.getmRecentInfo().langDest = actionLanguageDest
									.getItemAtPosition(pos).toString();
							readerContent.updateRecentInfo(readerContent
									.getmRecentInfo());

						}

					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {
					}
				});

		// TTS
		Locale[] locales = Locale.getAvailableLocales();
		List<String> localesArray = new ArrayList<String>();
		for (int l = 0; l < locales.length; l++) {
			Locale curLocale = locales[l];
			if (App.getApp().languages.has(curLocale.getLanguage())) {
				try {
					String niceName = App.getApp().languages
							.getString(curLocale.getLanguage());
					if (localesArray.contains(niceName) == false) {
						localesArray.add(niceName);
					}
				} catch (JSONException e) {

					e.printStackTrace();
				}
			}
		}
		String[] localesStringArray = localesArray
				.toArray(new String[localesArray.size()]);
		Arrays.sort(localesStringArray, String.CASE_INSENSITIVE_ORDER);

		ArrayAdapter<String> ttsLoclaes = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, localesStringArray);
		ttsLoclaes
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		this.spinTTSLocales.setAdapter(ttsLoclaes);

	}

	@Override
	protected void onDestroy() {
		if (tts != null) {
			tts.stop();
			tts.shutdown();
		}
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		super.onPause();

		this.readerContent
				.updateRecentInfo(this.readerContent.getmRecentInfo());
	}

	private RecentInfo readBookInfoFromDataBase(String bookDefaultName) {

		// Читаем инфо о книге
		RecentInfo tempRecent = null;
		DBFilter filter = new DBFilter();
		filter.addEntityItem("file", fileToRead.getAbsolutePath());
		ListOfObjects retList = RecentInfo.list(App.getApp().getDataBase()
				.openReadDataBase(), filter, RecentInfo.class);
		if (retList.data.size() > 0) {
			tempRecent = (RecentInfo) retList.data.get(0);
		} else {
			tempRecent = RecentInfo.fillBookInfo(App.getApp().getDataBase()
					.openWriteDataBase(), fileToRead, bookDefaultName);
		}

		return tempRecent;
	}

	@Override
	public void completeDocument(ReaderBookInfoDialog dialog, Document obj) {
		dialog.dismiss();
		String title = XMLXPathes.getXPathString(obj,
				XMLXPathes.FB2RWT_BOOK_TITLE);
		if (title == null) {
			title = fileToRead.getName();
		}

		readerContent.readFB2File(readBookInfoFromDataBase(title), obj);

		this.updateBookSettingsUI();

	}

	private void updateBookSettingsUI() {
		String nameDestCurrent = readerContent.getmRecentInfo().langDest == null ? App
				.getApp().langDest : readerContent.getmRecentInfo().langDest;
		String nameSourceCurrent = readerContent.getmRecentInfo().langSource == null ? App
				.getApp().langSource
				: readerContent.getmRecentInfo().langSource;

		for (int i = 0; i < actionLanguageDest.getCount(); i++) {
			if (actionLanguageDest.getItemAtPosition(i).toString()
					.equalsIgnoreCase(nameDestCurrent)) {
				actionLanguageDest.setSelection(i);
				break;
			}
		}

		for (int i = 0; i < actionLanguageSource.getCount(); i++) {
			if (actionLanguageSource.getItemAtPosition(i).toString()
					.equalsIgnoreCase(nameSourceCurrent)) {
				actionLanguageSource.setSelection(i);
				break;
			}
		}

		for (int i = 0; i < spinFontSize.getCount(); i++) {
			if (Integer.valueOf(spinFontSize.getItemAtPosition(i).toString()) == readerContent
					.getmRecentInfo().fontSize) {
				spinFontSize.setSelection(i);
				break;
			}
		}
		for (int i = 0; i < spinTTSLocales.getCount(); i++) {
			if (spinTTSLocales
					.getItemAtPosition(i)
					.toString()
					.equalsIgnoreCase(readerContent.getmRecentInfo().langSource)) {
				spinTTSLocales.setSelection(i);
				break;
			}
		}

	}

	@Override
	public void onClick(View v) {
		if (v.getId() == this.imgTranslateAction.getId()) {
			this.showOrHideActions("imgTranslateAction");
		} else if (v.getId() == this.imgBrightness.getId()) {
			this.showOrHideActions("imgBrightness");
		} else if (v.getId() == this.btnApplyBright.getId()) {
			this.applyBright();
		} else if (v.getId() == this.imgFontSize.getId()) {
			this.showOrHideActions("imgFontSize");
		} else if (v.getId() == this.imgTalk.getId()) {
			this.showOrHideActions("imgTalk");
		} else if (v.getId() == this.btnVoiceSelectedText.getId()) {
			this.voiceSelectedText();
		} else if (v.getId() == this.imgFontSize.getId()) {
			this.showOrHideActions("imgFontSize");

		} else if (v.getId() == this.btnBack.getId()) {
			this.closeReading();
		} else if (v.getId() == this.btnActionTranslate.getId()) {
			this.makeTextTranslate();
		} else if (v.getId() == this.imgShare.getId()) {
			this.shareText();
		} else if (v.getId() == this.btnApplyFontSize.getId()) {
			this.applyFontSize();
		} else if (v.getId() == this.imgDictionaries.getId()) {
			this.searchWordInDictionary();
		} else if (v.getId() == this.imgSearch.getId()) {
			this.searchTextInBook();
		} else if (v.getId() == this.imgCopy.getId()) {
			this.copyTextIntoClipboard();
		}

	}

	private void voiceSelectedText() {
		if (this.tts != null) {
			String langCode = getLangCode(this.spinTTSLocales.getSelectedItem()
					.toString());
			Locale foundLocale = getLocaleByCode(langCode);
			if (foundLocale != null) {
				tts.setLanguage(foundLocale);
				tts.speak(textInBuffer, TextToSpeech.QUEUE_FLUSH, null);
			}

		}

	}

	private void copyTextIntoClipboard() {
		ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
		clipboard.setText(textInBuffer);
		Toast.makeText(this, R.string.textCopied, Toast.LENGTH_LONG).show();

	}

	private void searchTextInBook() {
		final EditText editText = new EditText(this);
		new AlertDialog.Builder(this)
				.setTitle(R.string.searchInBook)
				.setView(editText)
				.setPositiveButton(R.string.search,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								if (editText.getText().length() > 0) {
									readerContent.searchTextInBook(editText
											.getText().toString());
								}
								dialog.dismiss();

							}
						})
				.setNegativeButton(R.string.rainprod_textCancel, null).create()
				.show();

	}

	private void searchWordInDictionary() {
		if (ColorDict3.isIntentAvailable(getApplicationContext(), new Intent(
				ColorDict3.SEARCH_ACTION))) {
			ColorDict3.searchWordInDict(this, this.textInBuffer);
		} else {
			new AlertDialog.Builder(this)
					.setTitle(R.string.notFoundColorDictTitle)
					.setMessage(R.string.notFoundColorDict)
					.setPositiveButton(R.string.download,
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									ColorDict3
											.openMarketForInstall(ReaderActivity.this);
									dialog.dismiss();
								}
							})
					.setNegativeButton(R.string.rainprod_textCancel, null)
					.create().show();
		}

	}

	private void applyFontSize() {
		readerContent.getmRecentInfo().fontSize = Integer.valueOf(spinFontSize
				.getSelectedItem().toString());
		readerContent.updateRecentInfo(readerContent.getmRecentInfo());
		readerContent.setFontSize(readerContent.getmRecentInfo().fontSize);
		this.showOrHideActions("");

		new AlertDialog.Builder(this)
				.setTitle(R.string.fontSetting)
				.setMessage(R.string.isRebuildBook)
				.setPositiveButton(R.string.update,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								readerContent.rebuildBookByFontSettings();

							}
						})
				.setNegativeButton(R.string.rainprod_textCancel, null).create()
				.show();

	}

	/**
	 * Применить яркость
	 */
	private void applyBright() {
		this.showOrHideActions("");

	}

	private void shareText() {
		Intent sharingIntent = new Intent(Intent.ACTION_SEND);
		sharingIntent.setType("text/html");

		sharingIntent.putExtra(
				android.content.Intent.EXTRA_TEXT,
				getString(R.string.shareText, this.textInBuffer, App.getApp()
						.getPackageName(), App.getApp().getPackageName()));
		sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
				getString(R.string.app_name));
		startActivity(Intent.createChooser(sharingIntent,
				Html.fromHtml(getString(R.string.textShare))));

	}

	private void makeTextTranslate() {

		this.service.translate(textInBuffer, this
				.getLangCode(this.readerContent.getmRecentInfo().langSource),
				this.getLangCode(this.readerContent.getmRecentInfo().langDest));

	}

	private void closeReading() {
		this.finish();

	}

	private void showOrHideActions(String newTag) {
		if (newTag.equalsIgnoreCase(this.currentTag)) {
			newTag = "";
		}
		this.currentTag = newTag;

		if (currentTag.equalsIgnoreCase("")) {
			actionsHolder.setVisibility(View.GONE);
		} else {
			actionsHolder.setVisibility(View.VISIBLE);

		}

		for (int c = 0; c < this.scrollViewLinearHolder.getChildCount(); c++) {
			View child = this.scrollViewLinearHolder.getChildAt(c);
			if (child.getTag().toString().equalsIgnoreCase(newTag)) {
				child.setVisibility(View.VISIBLE);

			} else {
				child.setVisibility(View.GONE);
			}
		}
	}

	@Override
	public void getLangs(String langs) {

	}

	@Override
	public void translatedText(String translatedText) {
		// 01-12 00:40:52.492: I/System.out(28735):
		// TEXT:{"code":200,"lang":"ru-en","text":["spear"]}
		AlertDialog adError = new AlertDialog.Builder(this)
				.setTitle(R.string.error).setMessage(R.string.errorTranslate)
				.create();
		try {
			JSONObject response = new JSONObject(translatedText);
			Integer code = response.getInt("code");
			if (code == 200) {
				JSONArray text = response.getJSONArray("text");
				String translate = text.getString(0);
				this.showAlertWithTranslateResult(translate);
			} else {
				adError.show();
			}
		} catch (JSONException e) {
			adError.show();
		}
		showOrHideActions("");
	}

	private void showAlertWithTranslateResult(final String translate) {
		RelativeLayout customTitle = new RelativeLayout(this);

		TextView textView = new TextView(this);
		textView.setTextColor(getResources().getColor(R.color.white));
		textView.setText(getString(R.string.translate));
		textView.setTextSize(getResources().getDimension(R.dimen.fontSize_16));

		Button btnVoice = new Button(this);
		btnVoice.setText(R.string.actionVoiceTranslate);
		btnVoice.setBackgroundResource(R.drawable.pretty_blue_bg_button);

		customTitle.addView(textView);

		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

		btnVoice.setLayoutParams(params); // causes layout update
		btnVoice.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Locale foundLocale = getLocaleByCode(getLangCode(readerContent
						.getmRecentInfo().langDest));
				if (foundLocale != null
						&& tts.isLanguageAvailable(foundLocale) == TextToSpeech.LANG_AVAILABLE) {
					voiceTranslatedText(translate);
				} else {
					if (translatedTextDialog != null
							&& translatedTextDialog.isShowing()) {
						translatedTextDialog.dismiss();
					}
					new AlertDialog.Builder(getApplicationContext())
							.setTitle(R.string.error)
							.setMessage(
									getString(
											R.string.localeTTSNotSupported,
											readerContent.getmRecentInfo().langDest))
							.setPositiveButton(R.string.rainprod_textOK,
									new DialogInterface.OnClickListener() {

										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {
											Intent installIntent = new Intent();
											installIntent
													.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
											startActivity(installIntent);

										}
									}).create().show();
				}

			}
		});
		customTitle.addView(btnVoice);

		LinearLayout messageLayout = new LinearLayout(this);
		TextView translatedText = new TextView(this);
		translatedText.setTextColor(getResources().getColor(R.color.white));
		translatedText.setText(translate);
		translatedText.setTextSize(Integer.parseInt(App.getApp()
				.getTranslatedTextFontSize()));

		// Yandex API License
		TextView yandexApiLicense = new TextView(this);
		yandexApiLicense.setTextColor(getResources().getColor(
				R.color.ligth_gray));
		yandexApiLicense
				.setText(Html
						.fromHtml("<a href=\"http://translate.yandex.com/\">Translated by the Yandex.Translate service</a> "));
		yandexApiLicense.setMovementMethod(LinkMovementMethod.getInstance());
		yandexApiLicense.setTextSize(getResources().getDimension(
				R.dimen.fontSize_10));

		messageLayout.setOrientation(LinearLayout.VERTICAL);
		messageLayout.addView(translatedText);
		messageLayout.addView(yandexApiLicense);
		ScrollView translateScroll = new ScrollView(this);
		translateScroll.addView(messageLayout);
		this.translatedTextDialog = new AlertDialog.Builder(this)
				.setView(translateScroll)
				.setCustomTitle(customTitle)
				.setPositiveButton(
						ru.rainprod.androidutils.R.string.rainprod_textOK,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								if (tts != null && tts.isSpeaking()) {
									tts.stop();
								}

							}
						}).create();
		this.translatedTextDialog.show();

	}

	private String getLangCode(String langPrettyName) {
		String retCode = "";
		JSONArray names = App.getApp().languages.names();

		for (int i = 0; i < names.length(); i++) {
			try {
				String niceName = App.getApp().languages.getString(names
						.getString(i));
				if (niceName.equalsIgnoreCase(langPrettyName)) {
					retCode = names.getString(i);
					break;
				}
			} catch (JSONException e) {
			}
		}
		return retCode;
	}

	private Locale getLocaleByCode(String localeCode) {
		Locale retLocale = null;
		Locale[] locales = Locale.getAvailableLocales();
		for (int l = 0; l < locales.length; l++) {
			Locale oneLocale = locales[l];
			if (oneLocale.getLanguage().equalsIgnoreCase(localeCode)) {
				retLocale = oneLocale;
				break;
			}
		}
		return retLocale;
	}

	/**
	 * Озвучка текста
	 * 
	 * @param translate
	 */
	protected void voiceTranslatedText(String translate) {
		String langCode = this
				.getLangCode(this.readerContent.getmRecentInfo().langDest);

		if (this.tts != null && langCode != null) {
			Locale foundLocale = getLocaleByCode(langCode);
			if (foundLocale != null) {
				tts.setLanguage(foundLocale);
				tts.speak(translate, TextToSpeech.QUEUE_FLUSH, null);
			}
		}

	}

	@Override
	public void onInit(int code) {
		if (code == TextToSpeech.SUCCESS) {
			tts.setLanguage(Locale.ENGLISH);

		} else {
			tts = null;
			Toast.makeText(this, "Failed to initialize TTS engine.",
					Toast.LENGTH_SHORT).show();
		}

	}
}
