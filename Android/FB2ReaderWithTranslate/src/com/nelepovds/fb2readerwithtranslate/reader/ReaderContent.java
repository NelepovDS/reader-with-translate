package com.nelepovds.fb2readerwithtranslate.reader;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.Html.ImageGetter;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Base64;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewFlipper;
import android.widget.ViewSwitcher;

import com.nelepovds.fb2readerwithtranslate.App;
import com.nelepovds.fb2readerwithtranslate.FB2XmlParser;
import com.nelepovds.fb2readerwithtranslate.FB2XmlParser.IFB2XmlParserSectionsListener;
import com.nelepovds.fb2readerwithtranslate.R;
import com.nelepovds.fb2readerwithtranslate.classes.RecentInfo;
import com.nelepovds.selectabletextviewer.SelectableTextViewer;

public class ReaderContent extends RelativeLayout implements
		View.OnClickListener {

	public static enum FB2RWT_SUPPORTED_FORMATS {
		FB2
	}

	public static final int FB2RWT_MIN_FONT_SIZE = 8;
	public static final int FB2RWT_MAX_FONT_SIZE = 100;

	private Integer fontSize = 14;
	private Integer fontColor = Color.BLACK;
	private Integer backgroundColor = Color.WHITE;

	public interface IReaderContentListener {

		public void haveCopiedData(String string);

		public void sectionWasCompletlyLoaded();

	}

	private IReaderContentListener mReaderContentListener;

	public void setReaderContentListener(
			IReaderContentListener readerContentListener) {
		this.mReaderContentListener = readerContentListener;
	}

	protected static final int FB2RWT_SECTIONS_UI = 1;
	private FB2RWT_SUPPORTED_FORMATS currentFormat = FB2RWT_SUPPORTED_FORMATS.FB2;

	private LinkedList<LinkedList<Node>> sections = new LinkedList<LinkedList<Node>>();

	private RecentInfo mRecentInfo;

	public RecentInfo getmRecentInfo() {
		return mRecentInfo;
	}

	/*
	 * UI Controls
	 */
	protected static final int FB2RWT_HTML_CONENT = 2;
	protected static final int FB2RWT_SEARCH_COMPLETE = 3;

	protected class FB2ParsingInfo {
		public Integer section;
		public Integer sectionsCount;
		public Integer sectionPart;
		public Integer sectionPartsCount;

		public FB2ParsingInfo(Integer section, Integer sectionsCount,
				Integer sectionPart, Integer sectionPartsCount) {
			super();
			this.section = section;
			this.sectionsCount = sectionsCount;
			this.sectionPart = sectionPart;
			this.sectionPartsCount = sectionPartsCount;
		}

	}

	protected static final int FB2RWT_PARSING_PROGRESS = 4;

	private ViewPager bookPages;
	private TextView textPaint;
	private SelectableTextViewer contentTextViewer;
	private RelativeLayout layoutContentHolder;
	private Integer scrollLastOpeningState = 0;
	private TextView txtTitleBookName;

	private AlertDialog mReadBookInfoDialog;

	private TextView mTextProgress;

	private Handler mUIHandler;

	// For text calculation
	private int mLineTextSizeHeight;
	private Rect mScrollRect;
	private ImageGetter mImageGetterForHtml;

	// For book controls
	private ImageView prevPage;
	private Button currentPage;
	private ImageView nextPage;
	private ProgressBar progressBook;
	private int mLineMaxChars;

	public ReaderContent(Context context) {
		super(context);
		if (!isInEditMode()) {
			createControl();
		}
	}

	public ReaderContent(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		if (!isInEditMode()) {
			createControl();
		}
	}

	public ReaderContent(Context context, AttributeSet attrs) {
		super(context, attrs);
		if (!isInEditMode()) {
			createControl();
		}
	}

	private void createControl() {
		LayoutInflater.from(getContext())
				.inflate(R.layout.reader_control, this);

		this.txtTitleBookName = (TextView) findViewWithTag("txtTitleBookName");
		this.txtTitleBookName.setText("");

		this.layoutContentHolder = (RelativeLayout) findViewWithTag("layoutContentHolder");
		this.initBookControls();
		this.createUIHandler();
		this.createHTMLImageGetter();
	}

	private void createBookPages() {
		this.bookPages = new ViewPager(getContext());
		this.bookPages.setAdapter(new PagerAdapter() {

			@Override
			public Object instantiateItem(ViewGroup container, int position) {
				View getView = createSelectedTextBlock(position);
				container.addView(getView, 0);
				return getView;
			}

			@Override
			public void destroyItem(ViewGroup container, int position,
					Object object) {
				container.removeView((View) object);

			}

			@Override
			public int getCount() {
				return sections.size();
			}

			@Override
			public boolean isViewFromObject(View arg0, Object arg1) {
				// TODO Auto-generated method stub
				return arg0 == ((View) arg1);
			}
		});
		this.bookPages
				.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

					@Override
					public void onPageSelected(int selectedPage) {
						sectionSelect(selectedPage);
					}

					@Override
					public void onPageScrolled(int arg0, float arg1, int arg2) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onPageScrollStateChanged(int arg0) {
						// TODO Auto-generated method stub

					}
				});
		this.bookPages.setCurrentItem(this.mRecentInfo.section, true);
		this.layoutContentHolder.addView(bookPages);

	}

	protected View createSelectedTextBlock(int position) {
		ScrollView retScrollContainer = new ScrollView(getContext());
		SelectableTextViewer selectableTextViewer = new SelectableTextViewer(
				getContext());
		selectableTextViewer.getTextView().setTextSize(
				this.mRecentInfo.fontSize);
		selectableTextViewer.getTextView().setTextColor(this.fontColor);
		retScrollContainer.addView(selectableTextViewer);

		StringBuilder sectionText = new StringBuilder();
		for (Node sectionChildLine : this.sections.get(position)) {
			if (sectionChildLine.getNodeType() == Element.ELEMENT_NODE) {
				System.out.println("ReaderContent TYPE:"
						+ sectionChildLine.getNodeType() + "  NAME:"
						+ sectionChildLine.getNodeName());
				String sectionPartHtmlElement = this
						.convertXmlNameToHtml(sectionChildLine.getNodeName());
				// Begin
				sectionText.append("<");
				sectionText.append(sectionPartHtmlElement);
				sectionText.append(">");
				// Content
				sectionText.append(sectionChildLine.getTextContent());
				// Closing
				sectionText.append("</");
				sectionText.append(sectionPartHtmlElement);
				sectionText.append(">");
			}

		}
		selectableTextViewer.setText((SpannableStringBuilder) Html
				.fromHtml(sectionText.toString()));
		return retScrollContainer;
	}

	private void initBookControls() {
		this.prevPage = (ImageView) findViewWithTag("prevPage");
		this.currentPage = (Button) findViewWithTag("currentPage");
		this.nextPage = (ImageView) findViewWithTag("nextPage");
		this.progressBook = (ProgressBar) findViewWithTag("progressBook");

		this.prevPage.setOnClickListener(this);
		this.currentPage.setOnClickListener(this);
		this.nextPage.setOnClickListener(this);

	}

	private void createHTMLImageGetter() {
		this.mImageGetterForHtml = new ImageGetter() {

			@Override
			public Drawable getDrawable(String source) {
				Drawable image = null;
				byte[] decodedString = Base64.decode(source, Base64.DEFAULT);
				final Bitmap decodedByte = BitmapFactory.decodeByteArray(
						decodedString, 0, decodedString.length);
				image = new BitmapDrawable(decodedByte);
				image.setBounds(0, 0, image.getIntrinsicWidth(),
						image.getIntrinsicHeight());
				return image;
			}
		};

	}

	public static final String FBRWT_ALPHABET = "QWERTYUIOPASDFGHJKLZXCVBNM";

	private void calculateSizesForTextWrap() {
		if (this.mScrollRect == null) {
			this.mScrollRect = new Rect();
			this.layoutContentHolder.getDrawingRect(mScrollRect);
		}
		this.mLineTextSizeHeight = textPaint.getLineHeight();

		TextPaint txtPaint = textPaint.getPaint();
		txtPaint.setSubpixelText(true);
		float total = 0.0f;
		for (int i = 0; i < FBRWT_ALPHABET.length(); i++) {
			String oneChar = FBRWT_ALPHABET.substring(i, i + 1);
			total += txtPaint.measureText(oneChar);
		}
		float avrgSize = total / FBRWT_ALPHABET.length();
		this.mLineMaxChars = (int) (this.mScrollRect.right / avrgSize);
	}

	private void createUIHandler() {
		this.mUIHandler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				switch (msg.what) {
				case FB2RWT_SECTIONS_UI:
					// Update book info
					txtTitleBookName.setText(mRecentInfo.name);
					scrollLastOpeningState = mRecentInfo.scrollSectionY;
					progressBook.setMax(sections.size());
					progressBook.setProgress(mRecentInfo.section);
					createBookPages();
					mReadBookInfoDialog.dismiss();
					break;
				case FB2RWT_HTML_CONENT:
					SpannableStringBuilder content = (SpannableStringBuilder) msg.obj;

					// contentTextViewer.setText(content);

					sectionWasCompletlyLoaded();
					mReadBookInfoDialog.dismiss();

					break;
				case FB2RWT_SEARCH_COMPLETE:
					Integer sectionFounded = (Integer) msg.obj;
					if (sectionFounded > -1) {
						sectionSelect(sectionFounded);
					}
					break;
				case FB2RWT_PARSING_PROGRESS:
					FB2ParsingInfo parsingInfo = (FB2ParsingInfo) msg.obj;
					mTextProgress.setText("Section:" + parsingInfo.section
							+ "/" + parsingInfo.sectionsCount + "\n"
							+ parsingInfo.sectionPart + "/"
							+ parsingInfo.sectionPartsCount);
					break;
				default:
					break;
				}
			}
		};

	}

	protected void sectionWasCompletlyLoaded() {
		if (this.mReaderContentListener != null) {
			this.mReaderContentListener.sectionWasCompletlyLoaded();
		}
		/*
		 * if (scrollLastOpeningState != 0) { scroll.postDelayed(new Runnable()
		 * { public void run() { scroll.smoothScrollTo(0,
		 * scrollLastOpeningState); scrollLastOpeningState = 0; } }, 500);
		 * 
		 * } else { scroll.smoothScrollTo(0, 0); }
		 */

	}

	private void createProgressDialog(String message) {
		if (this.mReadBookInfoDialog != null) {
			this.mReadBookInfoDialog.dismiss();
		}
		LinearLayout ll = new LinearLayout(getContext());
		ProgressBar pb = new ProgressBar(getContext());
		this.mTextProgress = new TextView(getContext());
		this.mTextProgress.setText(message);
		this.mTextProgress.setTextColor(Color.WHITE);
		this.mTextProgress.setGravity(Gravity.CENTER_VERTICAL);
		ll.addView(pb);
		ll.addView(this.mTextProgress, LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.MATCH_PARENT);
		this.mReadBookInfoDialog = new AlertDialog.Builder(getContext())
				.setCancelable(true).setView(ll).create();
		this.mReadBookInfoDialog.show();

	}

	public void readFB2File(RecentInfo pRecentInfo, Document pFB2Document) {
		createProgressDialog(getContext().getString(R.string.parsingFile));

		this.currentFormat = FB2RWT_SUPPORTED_FORMATS.FB2;
		this.mRecentInfo = pRecentInfo;
		this.mRecentInfo.section = this.mRecentInfo.section == null ? 0
				: this.mRecentInfo.section;
		this.mRecentInfo.fontSize = this.mRecentInfo.fontSize == null ? this.fontSize
				: this.mRecentInfo.fontSize;
		this.mRecentInfo.usePageBreaking = this.mRecentInfo.usePageBreaking == null ? 1
				: this.mRecentInfo.usePageBreaking;
		this.mRecentInfo.dateLastOpen = App.getTimeFormat(new Date());
		this.updateRecentInfo(mRecentInfo);
		// Заполняем данные о формате шрифта
		this.fontSize = this.mRecentInfo.fontSize;
		this.updateUI();
		calculateSizesForTextWrap();

		this.sections = new LinkedList<LinkedList<Node>>();
		new Thread(new Runnable() {
			public void run() {
				FB2XmlParser.parseSections(new File(mRecentInfo.file),
						new IFB2XmlParserSectionsListener() {

							@Override
							public void completeSection(Node sectionDom) {
								parseFB2OneSection(sectionDom);

							}

							@Override
							public void completeParse() {

								mUIHandler.sendEmptyMessage(FB2RWT_SECTIONS_UI);
							}
						});
			}
		}).start();

	}

	public static List<String> splitEqually(String text, int size) {
		// Give the list the right capacity to start with. You could use an
		// array
		// instead if you wanted.
		List<String> ret = new ArrayList<String>((text.length() + size - 1)
				/ size);

		for (int start = 0; start < text.length(); start += size) {
			ret.add(text.substring(start, Math.min(text.length(), start + size)));
		}
		return ret;
	}

	public static List<String> splitEqually(String fullText,
			TextPaint txtPaint, int maxWidth) {
		List<String> retList = new ArrayList<String>();
		while (fullText.length() > 0) {
			int brkLine = txtPaint.breakText(fullText, true, maxWidth, null);
			String tempString = fullText.substring(0, brkLine);
			retList.add(tempString);
			fullText = fullText.substring(brkLine);

		}

		return retList;
	}

	private void parseFB2OneSection(Node oneSection) {
		float adBanner = getContext().getResources().getDimension(
				R.dimen.adBanner);
		int maxLines = (int) ((this.mScrollRect.bottom - adBanner) / this.mLineTextSizeHeight) - 2;

		Date start = new Date();
		LinkedList<Node> sectionList = new LinkedList<Node>();
		int addedLines = 0;
		if (mRecentInfo.usePageBreaking == 1) {
			NodeList childrenOneSection = oneSection.getChildNodes();
			for (int i = 0; i < childrenOneSection.getLength(); i++) {

				Node partOfSection = childrenOneSection.item(i);
				if (partOfSection.getNodeType() == Element.ELEMENT_NODE) {
					if (partOfSection.getNodeName().equalsIgnoreCase(
							FB2XmlParser.FB2_SECTION_DATA_NAME)) {
						// Flush before
						if (sectionList.size() > 0) {// Если остались данные
							this.sections.add(sectionList);
							sectionList = new LinkedList<Node>();
							addedLines = 0;
						}

						parseFB2OneSection(partOfSection);
					} else {
						List<String> splitted = splitEqually(
								partOfSection.getTextContent(),
								this.mLineMaxChars);

						addedLines += splitted.size();
						if (addedLines >= maxLines) {
							sectionList.add(partOfSection);

							this.sections.add(sectionList);
							sectionList = new LinkedList<Node>();
							addedLines = 0;

						} else {
							sectionList.add(partOfSection);
						}
					}
				}
			}
		} else {
			// Не использовать переносы и построение текста
		}

		if (sectionList.size() > 0) {// Если остались данные
			this.sections.add(sectionList);
			sectionList = new LinkedList<Node>();
			addedLines = 0;
		}

		Date end = new Date();
		System.out.println("ReaderContent D:"
				+ (end.getTime() - start.getTime()));
	}

	/**
	 * Обновление информации об открытии файла
	 */
	public void updateRecentInfo(RecentInfo pNewRecentInfo) {
		this.mRecentInfo = pNewRecentInfo;
		// this.mRecentInfo.scrollSectionY = layoutContentHolder.getScrollY();
		this.mRecentInfo.totalSections = sections.size();
		ContentValues updVal = new ContentValues();
		updVal.put("file", this.mRecentInfo.file);
		this.mRecentInfo.update(updVal, App.getApp().getDataBase()
				.openWriteDataBase(), true);
	}

	private void sectionSelect(Integer sectionIndex) {
		sectionIndex = sectionIndex < 0 ? 0
				: (sectionIndex >= sections.size() ? sections.size() - 1
						: sectionIndex);
		this.mRecentInfo.section = sectionIndex;

		this.updateRecentInfo(this.mRecentInfo);

		this.updateUI();

	}

	private String convertXmlNameToHtml(String nodeName) {
		String retHtmlTag = nodeName;
		if (nodeName.equalsIgnoreCase("title")) {
			retHtmlTag = "h1";
		}

		return retHtmlTag;
	}

	@Override
	public void onClick(View v) {
		String curTag = v.getTag().toString();
		if (curTag.equalsIgnoreCase(this.prevPage.getTag().toString())) {
			this.goToPrevPage();
		} else if (curTag.equalsIgnoreCase(this.nextPage.getTag().toString())) {
			this.goToNextPage();
		} else if (curTag
				.equalsIgnoreCase(this.currentPage.getTag().toString())) {
			this.showDialogSelectPage();
		}

	}

	private void showDialogSelectPage() {
		Integer[] pagesArray = new Integer[this.sections.size()];
		for (int i = 0; i < this.sections.size(); i++) {
			pagesArray[i] = i + 1;
		}
		ArrayAdapter<Integer> pages = new ArrayAdapter<Integer>(getContext(),
				android.R.layout.simple_list_item_1, pagesArray);
		new AlertDialog.Builder(getContext())
				.setTitle(R.string.selectPage)
				.setSingleChoiceItems(pages, this.mRecentInfo.section,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								sectionSelect(which);
								dialog.dismiss();

							}
						}).create().show();

	}

	private void goToNextPage() {
		int nextPage = this.mRecentInfo.section;
		nextPage++;
		if (nextPage < this.sections.size()) {
			this.bookPages.setCurrentItem(nextPage, true);
		}

	}

	private void goToPrevPage() {
		int prevPage = this.mRecentInfo.section;
		prevPage--;
		if (prevPage >= 0) {
			this.bookPages.setCurrentItem(prevPage, true);

		}

	}

	private void updateUI() {
		if (this.fontSize == null || this.fontSize < FB2RWT_MIN_FONT_SIZE) {
			this.fontSize = FB2RWT_MIN_FONT_SIZE;
			this.mRecentInfo.fontSize = this.fontSize;
			this.updateRecentInfo(mRecentInfo);
		}
		textPaint = new TextView(getContext());
		textPaint.setTextSize(this.fontSize);
		textPaint.requestLayout();

		this.currentPage.setText(String.valueOf(this.sections.size() == 0 ? 0
				: this.mRecentInfo.section + 1)
				+ "/"
				+ String.valueOf(this.sections.size() == 0 ? 0 : this.sections
						.size()));
		this.progressBook.setProgress(this.mRecentInfo.section);

	}

	public Integer getFontSize() {
		return fontSize;
	}

	public void setFontSize(Integer fontSize) {
		this.fontSize = fontSize;
		this.updateUI();
	}

	public Integer getFontColor() {
		return fontColor;
	}

	public void setFontColor(Integer fontColor) {
		this.fontColor = fontColor;
		this.updateUI();
	}

	public Integer getBackgroundColor() {
		return backgroundColor;
	}

	public void setBackgroundColor(Integer backgroundColor) {
		this.backgroundColor = backgroundColor;
		this.updateUI();
	}

	public void rebuildBookByFontSettings() {
		createProgressDialog(getContext().getString(R.string.parsingFile));

		// Заполняем данные о формате шрифта
		this.fontSize = this.mRecentInfo.fontSize;
		this.updateUI();
		calculateSizesForTextWrap();
		new Thread(new Runnable() {
			public void run() {
				// Читаем тело

				mUIHandler.sendEmptyMessage(FB2RWT_SECTIONS_UI);

			}
		}).start();

	}

	public void searchTextInBook(final String searchString) {
		createProgressDialog(getContext().getString(R.string.searching));
		new Thread(new Runnable() {
			public void run() {
				Integer founded = -1;
				for (int i = 0; i < sections.size(); i++) {
					String sectionText = "";
					// TODO Вернуть поиск
					if (sectionText.contains(searchString.toLowerCase())) {
						founded = i;
						break;
					}
				}
				mUIHandler.sendMessage(mUIHandler.obtainMessage(
						FB2RWT_SEARCH_COMPLETE, founded));
			}
		}).start();

	}

}
