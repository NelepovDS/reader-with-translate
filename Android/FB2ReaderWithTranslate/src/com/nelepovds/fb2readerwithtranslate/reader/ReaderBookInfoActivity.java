package com.nelepovds.fb2readerwithtranslate.reader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.ext.DefaultHandler2;

import rainprod.utils.ui.OSUserInterface;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.Html.ImageGetter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.google.analytics.tracking.android.GoogleAnalytics;
import com.nelepovds.fb2readerwithtranslate.BaseActivity;
import com.nelepovds.fb2readerwithtranslate.FB2XmlParser;
import com.nelepovds.fb2readerwithtranslate.FB2XmlParser.IFB2XmlParserGetImageListener;
import com.nelepovds.fb2readerwithtranslate.R;
import com.nelepovds.fb2readerwithtranslate.XMLXPathes;
import com.nelepovds.fb2readerwithtranslate.dialog.ReaderBookInfoDialog;
import com.nelepovds.fb2readerwithtranslate.dialog.ReaderBookInfoDialog.IReaderBookInfoDialogListener;
import com.nelepovds.fb2readerwithtranslate.tools.FB2ReadTools;

public class ReaderBookInfoActivity extends BaseActivity implements
		OnClickListener {

	private Button btnBack;
	private Button btnBeginRead;

	private File fileToRead;

	private TextView txtTitle;
	private DefaultHandler2 saxPars;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_reader_book_info);
		OSUserInterface.initActivityControls(this);
		this.txtTitle.setText("");

		this.createProgressDialog(getString(R.string.opening));

		this.btnBack.setOnClickListener(this);
		this.btnBeginRead.setOnClickListener(this);

		this.fileToRead = new File(getIntent().getExtras().getString(
				ReaderActivity.FB2RWT_FILE_PATH));

		this.saxPars = new DefaultHandler2() {
			private Boolean founded = false;
			private StringBuilder sb = null;

			@Override
			public void startElement(String uri, String localName,
					String qName, Attributes attributes) throws SAXException {
				if (qName.equalsIgnoreCase("description")) {
					founded = true;
					sb = new StringBuilder();
				}
				if (sb != null) {
					sb.append("<");
					sb.append(qName);
					if (qName.equalsIgnoreCase("image")) {
						for (int a = 0; a < attributes.getLength(); a++) {
							sb.append(" ");
							sb.append(attributes.getLocalName(a));
							sb.append("=");
							sb.append("\"");
							sb.append(attributes.getValue(a));
							sb.append("\"");

						}
					}
					sb.append(">");
				}

			}

			@Override
			public void characters(char[] ch, int start, int length)
					throws SAXException {
				if (founded == true) {
					sb.append(new String(ch, start, length));
				}
			}

			@Override
			public void endElement(String uri, String localName, String qName)
					throws SAXException {
				if (sb != null) {
					sb.append("</" + qName + ">");
				}
				if (qName.equalsIgnoreCase("description")) {
					founded = false;
					parseDocXml(sb.toString());
					throw new SAXException("end");
				}

			}
		};

		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser parser;
		try {
			parser = factory.newSAXParser();
			FileInputStream fis = new FileInputStream(fileToRead);
			InputSource is = new InputSource(new InputStreamReader(fis,
					FB2ReadTools.getEncodingFile(fileToRead)));

			parser.parse(is, saxPars);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void parseDocXml(String docXml) {

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
			Document xmlBook = builder.parse(new InputSource(new StringReader(
					docXml)));
			completeDocument(xmlBook);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void onClick(View arg0) {
		if (arg0.getId() == this.btnBack.getId()) {
			this.finish();
		} else if (arg0.getId() == this.btnBeginRead.getId()) {

			new AlertDialog.Builder(this).setMessage(R.string.opening)
					.setCancelable(false).create().show();

			Intent readerIntent = new Intent(this, ReaderActivity.class);
			readerIntent.putExtra(ReaderActivity.FB2RWT_FILE_PATH,
					this.fileToRead.getAbsolutePath());

			startActivity(readerIntent);

			this.finish();
		}

	}

	public void completeDocument(final Document xmlBook) {

		new Thread(new Runnable() {

			@Override
			public void run() {

				final String title = XMLXPathes.getXPathString(xmlBook,
						XMLXPathes.FB2RWT_BOOK_TITLE);
				String imgCover = XMLXPathes.getXPathString(xmlBook,
						XMLXPathes.FB2RWT_COVER_IMAGE).replaceAll("#", "");
				System.out.println("ReaderBookInfoActivity title:" + title
						+ " \t imgCover:" + imgCover);

				if (imgCover.length() > 0) {
					FB2XmlParser.findFbImageByID(fileToRead, imgCover,
							new IFB2XmlParserGetImageListener() {

								@Override
								public void getImageBase64Content(
										String imageId, String contentBase64) {
									setDocTitle(title, contentBase64);
									progressInfoDialog.dismiss();
								}
							});
				} else {
					setDocTitle(title, "");
					progressInfoDialog.dismiss();
				}

			}

			private void setDocTitle(String title, String imgContent) {

				String htmlTitle = "<HTML>" + "<BODY>" + "<h1>" + title
						+ "</h1>"
						+ "<img style='width:100px;height:100px;' src=\""
						+ imgContent + "\" /></body></html>";

				final Spanned spanHtml = Html.fromHtml(htmlTitle,
						new ImageGetter() {

							@Override
							public Drawable getDrawable(String source) {
								Drawable image = null;
								byte[] decodedString = Base64.decode(source,
										Base64.DEFAULT);
								final Bitmap decodedByte = BitmapFactory
										.decodeByteArray(decodedString, 0,
												decodedString.length);
								image = new BitmapDrawable(decodedByte);
								image.setBounds(0, 0,
										image.getIntrinsicWidth(),
										image.getIntrinsicHeight());
								return image;
							}
						}, null);
				runOnUiThread(new Runnable() {
					public void run() {

						txtTitle.setText(spanHtml);

					}
				});

			}
		}).start();

	}
}
