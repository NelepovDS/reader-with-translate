package com.nelepovds.fb2readerwithtranslate.ideas;

import rainprod.utils.ui.OSUserInterface;

import com.nelepovds.fb2readerwithtranslate.R;

import android.app.Activity;
import android.os.Bundle;

public class IdeasOnlineBooks extends Activity {

	public static final String FB2RWT_ONLINE_BOOKS_URL = "http://moscow2013.hol.es/online_books.json";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ideas_online_books);
		OSUserInterface.initActivityControls(this);
	}

}
