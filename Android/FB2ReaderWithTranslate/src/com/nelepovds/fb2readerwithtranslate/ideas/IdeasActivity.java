package com.nelepovds.fb2readerwithtranslate.ideas;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Random;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import rainprod.utils.ui.OSUserInterface;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nelepovds.fb2readerwithtranslate.R;
import com.nelepovds.fb2readerwithtranslate.reader.ReaderActivity;
import com.nelepovds.fb2readerwithtranslate.reader.ReaderBookInfoActivity;

public class IdeasActivity extends Activity implements OnClickListener {

	private EditText editLinkForDownload;
	private Button btnDownloadByLink;

	private Button btnOpenBrowser;

	private Button btnShowOnlineSites;

	private long enqueue;

	private Random random;
	private BroadcastReceiver receiver;
	private Boolean stopDownloading = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ideas);
		OSUserInterface.initActivityControls(this);
		this.random = new Random();

		this.btnDownloadByLink.setOnClickListener(this);
		this.btnOpenBrowser.setOnClickListener(this);
		this.btnShowOnlineSites.setOnClickListener(this);

		String downloadFile = getIntent().getExtras().getString(
				ReaderActivity.FB2RWT_FILE_PATH);
		this.editLinkForDownload.setText(downloadFile);
		try {
			this.beginFileDownload();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void copy(File src, File dst) throws IOException {
		InputStream in = new FileInputStream(src);
		OutputStream out = new FileOutputStream(dst);

		// Transfer bytes from in to out
		byte[] buf = new byte[1024];
		int len;
		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}
		in.close();
		out.close();
	}

	protected void showAlerWithOpenFileQuestion(final File savedFile) {
		String mes = getString(R.string.fileSaveAtOpenIt,
				savedFile.getAbsolutePath());
		new AlertDialog.Builder(this)
				.setTitle(R.string.fileDownloaded)
				.setMessage(mes)
				.setPositiveButton(R.string.read,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								Intent readerIntent = new Intent(
										IdeasActivity.this,
										ReaderBookInfoActivity.class);
								readerIntent.putExtra(
										ReaderActivity.FB2RWT_FILE_PATH,
										savedFile.getAbsolutePath());
								startActivity(readerIntent);
								finish();

							}
						})
				.setNegativeButton(R.string.rainprod_textCancel,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								finish();

							}
						}).create().show();

	}

	@Override
	public void onClick(View arg0) {
		if (arg0.getId() == this.btnDownloadByLink.getId()) {

			try {
				this.beginFileDownload();
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else if (arg0.getId() == this.btnOpenBrowser.getId()) {
			this.openWebBrowserForSearch();
		} else if (arg0.getId() == this.btnShowOnlineSites.getId()) {
			this.showOnlineSites();
		}

	}

	private void showOnlineSites() {
		// TODO Auto-generated method stub

	}

	private void openWebBrowserForSearch() {
		String url = "http://www.google.com";
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(url));
		startActivity(i);

	}

	private void beginFileDownload() throws ClientProtocolException,
			IOException {
		if (this.editLinkForDownload.getText().toString().length() == 0) {
			new AlertDialog.Builder(this)
					.setTitle(R.string.error)
					.setMessage(R.string.errorDownloadEmptyLink)
					.setPositiveButton(
							ru.rainprod.androidutils.R.string.rainprod_textOK,
							null).create().show();
		} else {
			final String uri = this.editLinkForDownload.getText().toString();

			LinearLayout ll = new LinearLayout(this);
			ProgressBar pb = new ProgressBar(this);
			TextView mTextProgress = new TextView(this);
			mTextProgress.setText(getString(R.string.downloadingFile, uri, ""));
			mTextProgress.setTextColor(Color.WHITE);
			mTextProgress.setGravity(Gravity.CENTER_VERTICAL);
			ll.addView(pb);
			ll.addView(mTextProgress, LinearLayout.LayoutParams.WRAP_CONTENT,
					LinearLayout.LayoutParams.MATCH_PARENT);
			final AlertDialog mReadBookInfoDialog = new AlertDialog.Builder(
					this).setCancelable(true).setView(ll).create();
			mReadBookInfoDialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface arg0) {
							stopDownloading = true;
							finish();

						}
					});
			mReadBookInfoDialog.show();

			new Thread(new Runnable() {
				public void run() {
					try {
						stopDownloading = false;
						final File retfile = downloadFile(uri);
						if (retfile != null) {
							runOnUiThread(new Runnable() {
								public void run() {
									showAlerWithOpenFileQuestion(retfile);
									mReadBookInfoDialog.dismiss();
								}
							});
						}
					} catch (ClientProtocolException e) {
						e.printStackTrace();
						mReadBookInfoDialog.dismiss();
					} catch (IOException e) {
						e.printStackTrace();
						mReadBookInfoDialog.dismiss();
					}
				}
			}).start();

		}
	}

	public File downloadFile(String uri) throws ClientProtocolException,
			IOException {
		String fileName = Uri.parse(uri).getLastPathSegment()
				.replaceAll("/", "");
		File retFile = new File(Environment.getExternalStorageDirectory(),
				fileName);
		HttpParams httpParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParams, 15000);
		HttpConnectionParams.setSoTimeout(httpParams, 15000);
		DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);

		HttpGet httpGet = new HttpGet(uri);

		HttpResponse response = httpClient.execute(httpGet);
		HttpEntity entity = response.getEntity();
		if (entity != null) {
			OutputStream out = new FileOutputStream(retFile);

			DataInputStream dis = new DataInputStream(entity.getContent());
			byte[] buffer = new byte[1024];// In bytes
			int realyReaded;
			while ((realyReaded = dis.read(buffer)) > -1) {
				out.write(buffer, 0, realyReaded);
				if (stopDownloading == true) {

					break;
				}
			}
			out.close();

			dis.close();
		}
		if (stopDownloading == true) {
			return null;
		}
		return retFile;
	}
}
