package com.nelepovds.fb2readerwithtranslate;

import rainprod.utils.BaseObject;

public class ApplicationSettings extends BaseObject {
	/**
	 * Яркость
	 */
	public Double brigth;
	/**
	 * Цвет текста
	 */
	public Integer colorText;

	/**
	 * Размер шрифта
	 */
	public Double fontSize;
	
}
