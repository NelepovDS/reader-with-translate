package com.nelepovds.fb2readerwithtranslate.dialog;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Message;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

public class ReaderBookInfoDialog extends AlertDialog implements
		DialogInterface.OnCancelListener {

	protected static final int FB2RWT_READ_FILE = 1;

	protected static final int FB2RWT_ERROR = -1;

	protected static final int FB2RWT_COMPLETE = 10;
	private File fileToRead;
	private Handler uiHandler;

	public static interface IReaderBookInfoDialogListener {

		public void completeDocument(ReaderBookInfoDialog handler, Document obj);

	}

	private IReaderBookInfoDialogListener dialogListener;
	private TextView textProgress;

	public void setDialogListener(IReaderBookInfoDialogListener dialogListener) {
		this.dialogListener = dialogListener;
	}

	public ReaderBookInfoDialog(Context context) {
		super(context, true, null);
		LinearLayout ll = new LinearLayout(context);
		ProgressBar pb = new ProgressBar(context);
		this.textProgress = new TextView(context);
		this.textProgress.setText("");
		ll.addView(pb);
		ll.addView(this.textProgress);
		this.setView(ll);

		this.setOnCancelListener(this);
		this.uiHandler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				switch (msg.what) {
				case FB2RWT_READ_FILE:
					textProgress.setText("Открываю файл...");
					break;
				case FB2RWT_ERROR:
					textProgress.setText("Ошибка чтения книги");
					break;
				case FB2RWT_COMPLETE:
					dialogListener.completeDocument(ReaderBookInfoDialog.this,
							(Document) msg.obj);
					
					break;

				default:
					break;
				}

			}
		};
	}

	public ReaderBookInfoDialog readFile(File pFileToRead) {
		this.fileToRead = pFileToRead;
		new Thread(new Runnable() {
			public void run() {
				uiHandler.sendEmptyMessage(FB2RWT_READ_FILE);
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory
						.newInstance();
				dbFactory.setNamespaceAware(true);
				DocumentBuilder dBuilder;
				try {
					dBuilder = dbFactory.newDocumentBuilder();
					Document doc = dBuilder.parse(fileToRead);

					// optional, but recommended
					// read this -
					// http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
					//doc.getDocumentElement().normalize();
					
					uiHandler.sendMessage(uiHandler.obtainMessage(
							FB2RWT_COMPLETE, doc));
				} catch (ParserConfigurationException e) {
					uiHandler.sendMessage(uiHandler.obtainMessage(FB2RWT_ERROR,
							e));
				} catch (SAXException e) {
					uiHandler.sendMessage(uiHandler.obtainMessage(FB2RWT_ERROR,
							e));
				} catch (IOException e) {
					uiHandler.sendMessage(uiHandler.obtainMessage(FB2RWT_ERROR,
							e));
				}

			}
		}).start();
		this.show();
		return this;
	}

	@Override
	public void onCancel(DialogInterface dialog) {

	}

}
