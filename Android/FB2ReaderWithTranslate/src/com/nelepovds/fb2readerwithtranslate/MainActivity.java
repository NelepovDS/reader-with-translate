package com.nelepovds.fb2readerwithtranslate;

import java.io.File;
import java.util.Arrays;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;

import rainprod.utils.ListOfObjects;
import rainprod.utils.database.DBFilter;
import rainprod.utils.database.OrderItem;
import rainprod.utils.ui.OSUserInterface;
import rainprod.utils.ui.dialogs.OpenFileDialog;
import rainprod.utils.ui.dialogs.OpenFileDialog.OpenDialogListener;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Spinner;
import android.widget.TextView;

import com.nelepovds.fb2readerwithtranslate.classes.RecentInfo;
import com.nelepovds.fb2readerwithtranslate.ideas.IdeasActivity;
import com.nelepovds.fb2readerwithtranslate.reader.ReaderActivity;
import com.nelepovds.fb2readerwithtranslate.reader.ReaderBookInfoActivity;
import com.nelepovds.fb2readerwithtranslate.reader.ReaderContent;
import com.nelepovds.fb2readerwithtranslate.translate.TranslateAPIService;
import com.nelepovds.fb2readerwithtranslate.translate.TranslateAPIService.ITranslateAPIServiceListener;
import com.nelepovds.fb2readerwithtranslate.views.RecentView;
import com.nelepovds.fb2readerwithtranslate.views.RecentView.IRecentViewListener;

public class MainActivity extends Activity implements OnClickListener,
		ITranslateAPIServiceListener {

	private Button btnOpenFile;
	private Button btnRefreshLanguages;
	private Button btnClearRecent;
	private Button btnIdea;

	private Spinner langSource;

	private Spinner langDest;

	private LinearLayout recentOpened;

	private TranslateAPIService service;

	private LinearLayout mainLinear;

	private TextView txtExample;
	private Integer initFontSize;
	private Button btnUp;
	private Button btnDown;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		OSUserInterface.initActivityControls(this);

		this.service = new TranslateAPIService(this,
				getString(R.string.yandexAPIKey), this);

		this.btnOpenFile.setOnClickListener(this);
		this.btnRefreshLanguages.setOnClickListener(this);
		this.btnClearRecent.setOnClickListener(this);
		this.btnIdea.setOnClickListener(this);

		int top = (int) getResources().getDimension(R.dimen.grid4Size);
		for (int i = 0; i < mainLinear.getChildCount(); i++) {
			View getView = mainLinear.getChildAt(i);
			LinearLayout.LayoutParams ll = (LayoutParams) getView
					.getLayoutParams();
			ll.topMargin = top;
			getView.setLayoutParams(ll);
		}

		this.initControlsTranslatedTextFontSize();
		
	}

	private void initControlsTranslatedTextFontSize() {
		this.initFontSize = Integer.parseInt(App.getApp()
				.getTranslatedTextFontSize());
		this.txtExample.setTextSize(this.initFontSize);
		this.btnUp.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (initFontSize < ReaderContent.FB2RWT_MAX_FONT_SIZE) {
					initFontSize++;
				}
				txtExample.setTextSize(initFontSize);
				App.getApp().setTranslatedTextFontSize(initFontSize.toString());
			}
		});
		this.btnDown.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (initFontSize > ReaderContent.FB2RWT_MIN_FONT_SIZE) {
					initFontSize--;
				}
				txtExample.setTextSize(initFontSize);
				App.getApp().setTranslatedTextFontSize(initFontSize.toString());
			}
		});

	}

	@Override
	protected void onResume() {
		super.onResume();
		this.loadRecentFiles();
		if (App.getApp().dirs.size() > 0 && App.getApp().languages != null) {
			this.loadSpinners();
		} else {
			refreshLanguages();
		}

	}

	private void loadRecentFiles() {
		this.recentOpened.removeAllViews();

		DBFilter filter = new DBFilter();
		filter.addOrderItem("dateLastOpen", OrderItem.AU_ORDER_DESC);

		ListOfObjects listRecent = RecentInfo.list(App.getApp().getDataBase()
				.openReadDataBase(), filter, RecentInfo.class);

		for (int i = 0; i < listRecent.data.size(); i++) {
			RecentView recentView = new RecentView(LayoutInflater.from(this)
					.inflate(R.layout.item_recent, null));
			recentView.fillView((RecentInfo) listRecent.data.get(i));
			recentView.setListener(new IRecentViewListener() {

				@Override
				public void openForRead(RecentInfo info) {
					openReaderBookInfoActivity(info.file);
				}
			});
			this.recentOpened.addView(recentView.currentView);
		}
	}

	private void loadSpinners() {
		JSONArray names = App.getApp().languages.names();
		final String[] langs = new String[names.length()];
		for (int i = 0; i < names.length(); i++) {
			try {
				langs[i] = App.getApp().languages.getString(names.getString(i));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		Arrays.sort(langs, String.CASE_INSENSITIVE_ORDER);

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, langs);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		this.langSource.setAdapter(adapter);
		this.langDest.setAdapter(adapter);
		this.langSource
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int pos, long id) {
						App.getApp().setLangSource(langs[pos]);
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {
					}
				});
		this.langDest
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int pos, long id) {
						App.getApp().setLangDest(langs[pos]);
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {
					}
				});

		for (int i = 0; i < langs.length; i++) {
			String nameLang = langs[i];
			if (App.getApp().langDest.equalsIgnoreCase(nameLang)) {
				langDest.setSelection(i);
			}
			if (App.getApp().langSource.equalsIgnoreCase(nameLang)) {
				langSource.setSelection(i);
			}
		}

		this.checkIntentFromAndroidSystem();
	}

	private void checkIntentFromAndroidSystem() {
		if (getIntent() != null && getIntent().getAction() != null) {
			if (getIntent().getAction().equalsIgnoreCase(Intent.ACTION_VIEW)) {
				if (getIntent().getData() != null) {
					if (getIntent().getData().getScheme()
							.equalsIgnoreCase("file")) {

						File file = new File(getIntent().getData().getPath());

						if (file.exists()) {
							this.openReaderBookInfoActivity(file
									.getAbsolutePath());
						}
					} else if (getIntent().getData().getScheme()
							.startsWith("http")) {
						this.askToDownloadFileToLibrary(getIntent().getData());
					}
					setIntent(new Intent());
				}
			}
		}

	}

	private void askToDownloadFileToLibrary(final Uri intentData) {
		String mes = getString(R.string.isDownloadFile, intentData.getPath());
		final AlertDialog dialogAsk = new AlertDialog.Builder(this)
				.setTitle(R.string.download)
				.setMessage(mes)
				.setPositiveButton(R.string.download,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								Intent ideasLoad = new Intent(
										getApplicationContext(),
										IdeasActivity.class);
								ideasLoad.putExtra(
										ReaderActivity.FB2RWT_FILE_PATH,
										intentData.toString());
								startActivity(ideasLoad);
								dialog.dismiss();
								dialog.cancel();
							}
						})
				.setNegativeButton(R.string.rainprod_textCancel, null).create();
		dialogAsk.show();

	}

	@Override
	public void onClick(View arg0) {
		if (arg0.getId() == this.btnOpenFile.getId()) {
			this.onOpenFileClicked();
		} else if (arg0.getId() == this.btnRefreshLanguages.getId()) {
			this.refreshLanguages();
		} else if (arg0.getId() == this.btnClearRecent.getId()) {
			this.clearRecentHistory();
		} else if (arg0.getId() == this.btnIdea.getId()) {
			this.openIdeaActivity();
		}
	}

	private void openIdeaActivity() {
		Intent ideas = new Intent(this, IdeasActivity.class);
		startActivity(ideas);
	}

	private void clearRecentHistory() {
		ListOfObjects listRecent = RecentInfo.list(App.getApp().getDataBase()
				.openReadDataBase(), new DBFilter(), RecentInfo.class);

		for (int i = 0; i < listRecent.data.size(); i++) {
			RecentInfo oneInfo = (RecentInfo) listRecent.data.get(i);
			ContentValues removeVals = new ContentValues();
			removeVals.put("file", oneInfo.file);

			oneInfo.remove(removeVals, App.getApp().getDataBase()
					.openWriteDataBase(), true);
		}

		this.loadRecentFiles();

	}

	private void refreshLanguages() {
		this.service.getLangs(Locale.getDefault().getLanguage());

	}

	private void onOpenFileClicked() {
		System.out.println("DIR:"
				+ Environment.getExternalStorageDirectory().exists());
		OpenFileDialog fileDialog = new OpenFileDialog(this, Environment
				.getExternalStorageDirectory().getPath()).setFilter(".*\\.fb2")
				.setOpenDialogListener(new OpenDialogListener() {

					@Override
					public void OnSelectedFile(String fileName) {
						openReaderBookInfoActivity(fileName);

					}
				});
		fileDialog.show();
	}

	private void openReaderBookInfoActivity(String fileName) {
		Intent readerIntent = new Intent(this, ReaderBookInfoActivity.class);
		readerIntent.putExtra(ReaderActivity.FB2RWT_FILE_PATH, fileName);
		startActivity(readerIntent);
	}

	@Override
	public void getLangs(String langs) {
		try {
			App.getApp().updateLanguagesInfo(langs);
			loadSpinners();
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void translatedText(String translatedText) {

	}

}
