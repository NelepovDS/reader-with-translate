package com.nelepovds.selectabletextviewer;

import com.nelepovds.selectabletextviewer.SelectableTextViewer.ISelectableTextViewerListener;

import android.os.Bundle;
import android.app.Activity;
import android.graphics.Color;
import android.text.Html;
import android.text.InputType;
import android.text.Spannable;
import android.text.Spannable.Factory;
import android.text.SpannableStringBuilder;
import android.text.method.ArrowKeyMovementMethod;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView.BufferType;

public class MainActivity extends Activity {

	public SelectableTextViewer textViewer;
	public ScrollView scroll;

	public String someText = "Инструменты для проверки компоновки пользовательского интерфейса"
			+ "Инструмент layoutopt"
			+ "Прежде чем завершить работу над приложением, я всегда проверяю, нет ли простых возможностей для повышения производительности за счет изменения компоновки пользовательского интерфейса. Инструмент layoutopt позволяет проанализировать файлы компоновки и найти потенциальные проблемы, влияющие на производительность. Можно ознакомиться с блогом и справочным документом, где этот инструмент описан более подробно, а в данной статье мы просто кратко продемонстрируем его использование. Командная строка выглядит следующим образом:";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_main);
		this.textViewer = (SelectableTextViewer) findViewById(R.id.selectableTextViewer1);

		this.scroll = (ScrollView) findViewById(R.id.scroll);
		// this.textViewer.setInsideScrollView(scroll);

		SpannableStringBuilder builder = new SpannableStringBuilder(someText
				+ someText + someText + someText + someText + someText
				+ someText + someText + someText + someText + someText
				+ someText + someText + someText);
		this.textViewer.setText((SpannableStringBuilder) Html.fromHtml(someText
				+ someText + someText + someText + someText + someText
				+ someText + someText + someText + someText + someText
				+ someText + someText + someText));

	}
}
