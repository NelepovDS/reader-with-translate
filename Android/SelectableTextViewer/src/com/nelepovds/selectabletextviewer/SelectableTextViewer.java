package com.nelepovds.selectabletextviewer;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.BackgroundColorSpan;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.BufferType;

public class SelectableTextViewer extends RelativeLayout {

	private ImageView imgStartSelect;
	private int mStartSelect = -1;
	private ImageView imgEndSelect;
	private int mEndSelect = -1;
	private int mImgWidth = 40;
	private int mImgHeight = 50;
	private boolean isWordSelection = false;

	public void setWordSelection(boolean isWordSelection) {
		this.isWordSelection = isWordSelection;
	}

	public boolean isWordSelection() {
		return isWordSelection;
	}

	private TextView textView;

	private View mCurrentControlFocused;

	public static interface ISelectableTextViewerListener {

		public void updateSelection(SelectableTextViewer selectableTextViewer);

		public void endSelectingText(SelectableTextViewer selectableTextViewer,
				String selectedText);

		public void stopSelectingText(
				SelectableTextViewer selectableTextViewer, String selectedText);

	}

	private ISelectableTextViewerListener selectableTextViewerListener;
	private BackgroundColorSpan spanBackgroundColored;
	protected int[] controlLocation;

	public void setSelectableTextViewerListener(
			ISelectableTextViewerListener selectableTextViewerListener) {
		this.selectableTextViewerListener = selectableTextViewerListener;
	}

	public SelectableTextViewer(Context context) {
		super(context);
		if (!isInEditMode()) {
			this.initControls();
		}
	}

	public SelectableTextViewer(Context context, AttributeSet attrs) {
		super(context, attrs);
		if (!isInEditMode()) {
			this.initControls();
		}
	}

	public SelectableTextViewer(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		if (!isInEditMode()) {
			this.initControls();
		}
	}

	private void initControls() {
		this.spanBackgroundColored = new BackgroundColorSpan(Color.LTGRAY);
		this.textView = new TextView(getContext());
		this.addView(textView);
		this.setOnLongClickListener(new TextView.OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				stopSelecting();
				showSelectionControls();
				controlLocation = new int[] { 0, 0 };

				getLocationOnScreen(controlLocation);
				if (selectableTextViewerListener != null) {
					selectableTextViewerListener.endSelectingText(
							SelectableTextViewer.this, getSelectedText());
				}
				return false;
			}
		});

		this.createImgControllersForSelection();

	}

	protected void disallowIntercept(Boolean disallowIntercept) {
		this.getParent().requestDisallowInterceptTouchEvent(disallowIntercept);
	}

	protected void createImgControllersForSelection() {
		this.imgStartSelect = new ImageView(getContext());
		this.imgEndSelect = new ImageView(getContext());
		this.imgStartSelect.setImageResource(R.drawable.cursor);
		this.imgEndSelect.setImageResource(R.drawable.cursor);
		this.addView(imgStartSelect, mImgWidth, mImgHeight);
		this.addView(imgEndSelect, mImgWidth, mImgHeight);
		OnClickListener onClickForChangeFocus = new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				mCurrentControlFocused = v;
			}
		};
		this.imgEndSelect.setOnClickListener(onClickForChangeFocus);
		this.imgStartSelect.setOnClickListener(onClickForChangeFocus);

		View.OnTouchListener onTouchSelectionControl = new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				disallowIntercept(true);

				mCurrentControlFocused = v;
				int eid = event.getAction();
				switch (eid) {
				case MotionEvent.ACTION_MOVE:

					RelativeLayout.LayoutParams mParams = (RelativeLayout.LayoutParams) v
							.getLayoutParams();

					int x = (int) event.getRawX();
					int y = (int) event.getRawY();

					mParams.leftMargin = x - mImgWidth / 2 - controlLocation[0];
					if (x <= 0) {
						mParams.leftMargin = mImgWidth;
					} else if (x > (getMeasuredWidth() - mImgWidth)) {
						mParams.leftMargin = getMeasuredWidth() - mImgWidth;
					}

					mParams.topMargin = (int) (y - (controlLocation[1] + mImgHeight * 1.5f));
					if (mParams.topMargin <= 1) {
						mParams.topMargin = 1;
					}
					v.setLayoutParams(mParams);
					updateSelectionByMovementImgControls(mParams.leftMargin,
							mParams.topMargin);
					break;
				case MotionEvent.ACTION_UP:
					if (selectableTextViewerListener != null) {
						selectableTextViewerListener.endSelectingText(
								SelectableTextViewer.this, getSelectedText());
					}

					break;
				default:
					disallowIntercept(false);
					break;
				}
				return true;

			}
		};

		this.imgEndSelect.setOnTouchListener(onTouchSelectionControl);
		this.imgStartSelect.setOnTouchListener(onTouchSelectionControl);

		this.imgEndSelect.setVisibility(View.GONE);
		this.imgStartSelect.setVisibility(View.GONE);

	}

	protected void updateSelectionByMovementImgControls(int x, int y) {
		if (mCurrentControlFocused.equals(imgStartSelect)) {
			this.mStartSelect = getOffsetByCoordinates(x + mImgWidth / 2, y);
		} else if (mCurrentControlFocused.equals(imgEndSelect)) {
			this.mEndSelect = getOffsetByCoordinates(x + mImgWidth / 2, y);
		}

		if (this.isWordSelection) {
			onTouchDownCalcSelections(x, y);
		}
		updateSelectionSpan();

	}

	protected Layout updateSelectionSpan() {
		Layout retLayout = this.textView.getLayout();

		if (this.mStartSelect > -1 && this.mEndSelect > -1) {
			if (this.mStartSelect > this.mEndSelect) {
				int temp = mEndSelect;
				this.mEndSelect = mStartSelect;
				this.mStartSelect = temp;
				showSelectionControls();
			}

			Spannable s = (Spannable) this.textView.getText();
			s.removeSpan(this.spanBackgroundColored);
			s.setSpan(this.spanBackgroundColored, this.mStartSelect,
					this.mEndSelect, Spannable.SPAN_USER);
			if (this.selectableTextViewerListener != null) {
				this.selectableTextViewerListener.updateSelection(this);
			}
		}
		return retLayout;
	}

	protected void showSelectionControls() {
		if (this.mStartSelect > -1 && this.mEndSelect > -1) {
			Layout layout = updateSelectionSpan();

			Rect parentTextViewRect = new Rect();

			RelativeLayout.LayoutParams startLP = (LayoutParams) this.imgStartSelect
					.getLayoutParams();
			float xStart = layout.getPrimaryHorizontal(this.mStartSelect)
					- mImgWidth / 2;
			float yStart = layout.getLineBounds(
					layout.getLineForOffset(this.mStartSelect),
					parentTextViewRect);
			startLP.setMargins((int) xStart, (int) yStart, -1, -1);

			this.imgStartSelect.setLayoutParams(startLP);
			this.imgStartSelect.setVisibility(View.VISIBLE);

			RelativeLayout.LayoutParams endLP = (LayoutParams) this.imgEndSelect
					.getLayoutParams();
			float xEnd = layout.getPrimaryHorizontal(this.mEndSelect)
					- mImgWidth / 2;
			float yEnd = layout.getLineBounds(
					layout.getLineForOffset(this.mEndSelect),
					parentTextViewRect);
			endLP.setMargins((int) xEnd, (int) yEnd, -1, -1);
			this.imgEndSelect.setLayoutParams(endLP);
			this.imgEndSelect.setVisibility(View.VISIBLE);

		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			if (this.imgStartSelect != null) {
				if (this.imgStartSelect.getVisibility() == View.GONE) {

					this.onTouchDownCalcSelections(event.getX(), event.getY());
					disallowIntercept(false);

				} else {
					this.stopSelecting();

				}
			}
		} else {
			this.disallowIntercept(false);
		}
		return super.onTouchEvent(event);
	}

	private void hideSelectionControls() {
		this.imgStartSelect.setVisibility(View.GONE);
		this.imgEndSelect.setVisibility(View.GONE);

	}

	private int getOffsetByCoordinates(float x, float y) {
		int retOffset = -1;
		Layout layout = this.textView.getLayout();
		if (layout != null) {
			int line = layout.getLineForVertical((int) y);
			retOffset = layout.getOffsetForHorizontal(line, x);
		}
		return retOffset;
	}

	private void onTouchDownCalcSelections(float x, float y) {
		this.mStartSelect = getOffsetByCoordinates(x, y);
		if (this.mStartSelect > -1) {
			// Calculate text end
			String tempStr = this.textView.getText().toString();
			tempStr = tempStr.substring(this.mStartSelect);
			Pattern pt = Pattern.compile("[^a-z\u0400-\u04FF]");
			Matcher mt = pt.matcher(tempStr);
			if (mt.find()) {
				String match = mt.group(0);
				tempStr = tempStr.substring(0, tempStr.indexOf(match));
			}
			this.mEndSelect = this.mStartSelect + tempStr.length();
			// Now recalculate text start
			tempStr = this.textView.getText().toString();
			for (int i = this.mEndSelect - 1; i > 0; i--) {
				String newString = tempStr.substring(i, mEndSelect);
				mt = pt.matcher(newString);
				if (mt.find()) {
					this.mStartSelect = i + 1;
					break;
				}
			}
		}
	}

	public void setText(SpannableStringBuilder builder) {
		this.textView.setText(builder, BufferType.SPANNABLE);
		this.hideSelectionControls();

	}

	public ImageView getImgEndSelect() {
		return imgEndSelect;
	}

	public ImageView getImgStartSelect() {
		return imgStartSelect;
	}

	/**
	 * For this all doing
	 * 
	 * @return
	 */
	public String getSelectedText() {
		String retSelectedString = null;
		if (this.mStartSelect > -1 && this.mEndSelect > -1) {
			retSelectedString = this.textView.getText()
					.subSequence(this.mStartSelect, this.mEndSelect).toString();
		}
		return retSelectedString;
	}

	/**
	 * Hides cursors and clears
	 * 
	 * @return
	 */
	public void stopSelecting() {
		this.hideSelectionControls();
		Spannable spanable = (Spannable) this.textView.getText();
		spanable.removeSpan(this.spanBackgroundColored);
		if (selectableTextViewerListener != null) {
			selectableTextViewerListener.stopSelectingText(
					SelectableTextViewer.this, getSelectedText());
		}
	}

	public TextView getTextView() {
		return textView;
	}

	public void setTextView(TextView textView) {
		this.textView = textView;
	}

	public BackgroundColorSpan getSpanBackgroundColored() {
		return spanBackgroundColored;
	}

	public void setSpanBackgroundColored(
			BackgroundColorSpan spanBackgroundColored) {
		this.spanBackgroundColored = spanBackgroundColored;
	}

	public void setImgStartSelect(ImageView imgStartSelect) {
		this.imgStartSelect = imgStartSelect;
	}

	public void setImgEndSelect(ImageView imgEndSelect) {
		this.imgEndSelect = imgEndSelect;
	}

}
